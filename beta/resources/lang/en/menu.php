<?php

return [

    'dashboard' => 'Dashboard',
    'user' => 'User',
    'profile' => 'My Profile',
    'list_user' => 'List User',
    'list_roles' => 'List Roles',
    'list_permissions' => 'List Permissions',
    'post' => 'Articles',
    'list_post' => 'List Article',
    'live' => 'Live Schedule',
    'live_schedule' => 'Live Schedule List',
    'film' => 'Film',
    'list_film' => 'List Films',
    'media' => 'File Media',
    'post' => 'List Post' ,
    'new-post' => 'Upload Post' ,
    'local_media' => 'Local Media',
    'cloud_media' => 'Cloud Media',
    'list_media' => 'List File Media',
    'category' => 'Category',
    'option' => 'Option',
    'sms' => 'SMS Content',
    'list_sms' => 'List SMS Content',
    'create_sms_content' => 'Create SMS Content',
    'report' => 'Report' ,
    'upload_media' => 'Upload Media' ,
    'list_media' => 'List Medias'

];
