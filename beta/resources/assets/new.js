{
    "_id" : ObjectId("566f9406dc604194f9375bba"),
    "service_id" : "HALOSOKA",
    "cp_code" : "TtVang",
    "service_code" : "HALOSOKA",
    "short_code" : "15602",
    "packages" : {
    "1" : {
        "package_code" : "TIP_C1",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "2" : {
        "package_code" : "TIP_C2",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "3" : {
        "package_code" : "TIP_PL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "4" : {
        "package_code" : "TIP_LL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "5" : {
        "package_code" : "TIP_SL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "6" : {
        "package_code" : "TIP_BL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "7" : {
        "package_code" : "LIVE_C1",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "8" : {
        "package_code" : "LIVE_C2",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "9" : {
        "package_code" : "LIVE_PL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "10" : {
        "package_code" : "LIVE_LL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "11" : {
        "package_code" : "LIVE_SL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "12" : {
        "package_code" : "LIVE_BL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "13" : {
        "package_code" : "NEWS_C1",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "14" : {
        "package_code" : "NEWS_C2",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "15" : {
        "package_code" : "NEWS_PL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "16" : {
        "package_code" : "NEWS_LL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "17" : {
        "package_code" : "NEWS_SL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "18" : {
        "package_code" : "NEWS_BL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : ""
    },
    "19" : {
        "package_code" : "HELP",
            "description" : "",
            "price" : NumberLong(0),
            "period" : NumberLong(1),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0)
    },
    "20" : {
        "package_code" : "HOT_C1",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_C1"
    },
    "21" : {
        "package_code" : "HOT_C2",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_C2"
    },
    "22" : {
        "package_code" : "HOT_LL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_LL"
    },
    "23" : {
        "package_code" : "HOT_BL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_BL"
    },
    "24" : {
        "package_code" : "HOT_SL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_SL"
    },
    "25" : {
        "package_code" : "HOT_PL",
            "period" : NumberLong(1),
            "price" : NumberLong(0),
            "owe_max_day" : NumberLong(30),
            "promotion_day_for_1time" : NumberLong(0),
            "description" : "HOT_PL"
    }
},
    "mos" : [
    {
        "content" : "HELP",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "mo",
        "package_code" : "HELP",
        "mts_error" : {
            "FORWARD_PROCESS_FAIL" : "He thong dang ban.Vui long thu lai sau.Truy cap http://tuyengap.vn de biet them chi tiet"
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou can enjoy most interesting football content including news, video, TIP... by using HALO SOKA service.\r\nTo get live information, send LIVE <LeagueCode> to 15602 for registering the service.\r\nTo get attractive tips, send TIP <LeagueCode> to 15602 for registering the service.\r\nTo get exciting news, send NEWS <LeagueCode> to 15602 for registering the service.\r\nTo get hot update, send HOT <LeagueCode> to 15602 for registering the service.\r\nLeague codes list:\r\nC1: Champion League\r\nC2: Europa League\r\nPL: Premier League\r\nLL: La Liga League\r\nSL: Serie A League\r\nBL: Bundesliga League\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP C1 to 15602.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of UEFA Champions league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP C2 to 15602.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Europa league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of  Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of England Premier league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Laliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of SerieA league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "TIP BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "TIP_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Bundesliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP C1 to 15602.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : "[HALO SOKA]\roffnSystem is busy. Please comeback later.Thank you"
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of UEFA Champions league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP C1 to 15602.\r\nEveryday, you will get the attractive tips of UEFA Champions league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP C2 to 15602.\r\nEveryday, you will get the attractive tips of Europa league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Europa league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP C2 to 15602.\r\nEveryday, you will get the attractive tips of  Europa league.\r\nTo register the daily package, send TIP C2 to 15602.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP PL to 15602.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of England Premier league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP PL to 15602.\r\nEveryday, you will get the attractive tips of England Premier league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP LL to 15602.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Laliga league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP PL to 15602.\r\nTo register the daily package, send TIP LL to 15602.\r\nEveryday, you will get the attractive tips of Laliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP SL to 15602.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of SerieA league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP SL to 15602.\r\nEveryday, you will get the attractive tips of SerieA league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF TIP BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "TIP_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send TIP BL to 15602.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Bundesliga league of HALO SOKA service successfully.\r\nTo register the daily package, send TIP BL to 15602.\r\nEveryday, you will get the attractive tips of Bundesliga league.\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE C1 to 15602.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of UEFA Champions league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the live information of Europa league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Europa league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Europa league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of Europa league .\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of England Premier league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Laliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of SerieA league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "LIVE BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "LIVE_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the live information of Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Bundesliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the live information of  Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE C1 to 15602.\r\nEveryday, you will get the live information  of C1 league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of UEFA Champions league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE C1 to 15602.\r\nEveryday, you will get the live information of UEFA Champions league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the live information of Europa league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Europa league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE C2 to 15602.\r\nEveryday, you will get the live information  of Europa league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Europa league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE C2 to 15602.\r\nEveryday, you will get the live information of Europa league .\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE PL to 15602.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of England Premier league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE PL to 15602.\r\nEveryday, you will get the live information of England Premier league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE LL to 15602.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Laliga league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE LL to 15602.\r\nEveryday, you will get the live information of Laliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE SL to 15602.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of SerieA league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE SL to 15602.\r\nEveryday, you will get the live information of SerieA league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF LIVE BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "LIVE_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the live information of Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the live information of Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send LIVE BL to 15602.\r\nEveryday, you will get the live information of Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Bundesliga league of HALO SOKA service successfully.\r\nTo register the daily package, send LIVE BL to 15602.\r\nEveryday, you will get the live information of  Bundesliga league.\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS C1 to 15602.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of UEFA Champions league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Europa league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of England Premier league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Laliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of SerieA league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "NEWS BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "register",
        "package_code" : "NEWS_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Bundesliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS C1",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS C1 to 15602.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of UEFA Champions league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS C1 to 15602.\r\nEveryday, you will get the exciting news of UEFA Champions league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS C2",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS C2 to 15602.\r\nEveryday, you will get the exciting news of Europa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Europa league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS C2 to 15602.\r\nEveryday, you will get the exciting news ofEuropa league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS PL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS PL to 15602.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of England Premier league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS PL to 15602.\r\nEveryday, you will get the exciting news of England Premier league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS LL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS LL to 15602.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Laliga league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS LL to 15602.\r\nEveryday, you will get the exciting news of Laliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS SL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS SL to 15602.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of SerieA league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS SL to 15602.\r\nEveryday, you will get the exciting news of SerieA league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "OFF NEWS BL",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "NEWS_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send NEWS BL to 15602.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Bundesliga league of HALO SOKA service successfully.\r\nTo register the daily package, send NEWS BL to 15602.\r\nEveryday, you will get the exciting news of Bundesliga league.\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "HELP TIP",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "mo",
        "package_code" : "HELP",
        "mts_error" : {
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nIt allows HALOTEL subscribers to get the attractive tips of all popular leagues in daily.\r\nTo register the daily package, send TIP <LeagueCode> to 15602 (Fee:100 Tsh).To cancel the package, send OFF TIP <LeagueCode> to 15602.\r\nLeague codes list:\r\nC1:Champions league\r\nC2:Europa League\r\nPL:Premier League\r\nLL:La Liga League\r\nSL:Serie A League\r\nBL:Bundesliga League\r\nFor detail information, please send HELP TIP to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "HELP NEWS",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "mo",
        "package_code" : "HELP",
        "mts_error" : {
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nIt allows HALOTEL subscribers to get the exciting news of all popular leagues in daily.\r\nTo register the daily package, send NEWS <LeagueCode> to 15602 (Fee:100 Tsh).To cancel the package, send OFF NEWS <LeagueCode> to 15602.\r\nLeague codes list:\r\nC1:Champions league\r\nC2:Europa League\r\nPL:Premier League\r\nLL:La Liga League\r\nSL:Serie A League\r\nBL:Bundesliga League\r\nFor detail information, please send HELP NEWS to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "HELP LIVE",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "mo",
        "package_code" : "HELP",
        "mts_error" : {
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nIt allows HALOTEL subscribers to get the live information of all popular leagues in daily.\r\nTo register the daily package, send LIVE <LeagueCode> to 15602 (Fee:100 Tsh).To cancel the package, send OFF LIVE <LeagueCode> to 15602.\r\nLeague codes list:\r\nC1:Champions league\r\nC2:Europa League\r\nPL:Premier League\r\nLL:La Liga League\r\nSL:Serie A League\r\nBL:Bundesliga League\r\nFor detail information, please send HELP LIVE to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "HELP HOT",
        "description" : "",
        "price" : NumberLong(0),
        "type" : "mo",
        "package_code" : "HELP",
        "mts_error" : {
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nIt allows HALOTEL subscribers to get the hot update of all popular leagues in daily.\r\nTo register the daily package, send HOT <LeagueCode> to 15602 (Fee:100 Tsh).To cancel the package, send OFF HOT <LeagueCode> to 15602.\r\nLeague codes list:\r\nC1:Champions league\r\nC2:Europa League\r\nPL:Premier League\r\nLL:La Liga League\r\nSL:Serie A League\r\nBL:Bundesliga League\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "use_check_mode" : NumberLong(0),
        "support_action_on_wap" : NumberLong(1)
    },
    {
        "content" : "HOT C1",
        "price" : NumberLong(0),
        "description" : "HOT_C1",
        "type" : "register",
        "package_code" : "HOT_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of UEFA Champions league league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT C1 to 15602.\r\nEveryday, you will get the hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of UEFA Champions league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT C1",
        "description" : "HOT_C1",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_C1",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of UEFA Champions league of HALO SOKA service.\r\nEveryday, you will get the hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of UEFA Champions league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of UEFA Champions league league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT C1 to 15602.\r\nEveryday, you will get the hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of UEFA Champions league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT C1 to 15602.\r\nEveryday, you will get the hot update of UEFA Champions league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "HOT C2",
        "price" : NumberLong(0),
        "description" : "HOT_C2",
        "type" : "register",
        "package_code" : "HOT_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Europa league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT C2",
        "description" : "HOT_C2",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_C2",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Europa league of HALO SOKA service.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Europa league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT C2 to 15602.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Europa league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT C2 to 15602.\r\nEveryday, you will get the hot update of Europa league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "HOT LL",
        "price" : NumberLong(0),
        "description" : "HOT_LL",
        "type" : "register",
        "package_code" : "HOT_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Laliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT LL",
        "description" : "HOT_LL",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_LL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Laliga league of HALO SOKA service.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Laliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT LL to 15602.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Laliga league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT LL to 15602.\r\nEveryday, you will get the hot update of Laliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "HOT BL",
        "price" : NumberLong(0),
        "description" : "HOT_BL",
        "type" : "register",
        "package_code" : "HOT_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT BL to 15602.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of Bundesliga league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT_BL",
        "description" : "HOT_BL",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_BL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of Bundesliga league of HALO SOKA service.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of Bundesliga league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT BL to 15602.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of Bundesliga league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT BL to 15602.\r\nEveryday, you will get the hot update of Bundesliga league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "HOT SL",
        "price" : NumberLong(0),
        "description" : "HOT_SL",
        "type" : "register",
        "package_code" : "HOT_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of SerieA league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT SL",
        "description" : "HOT_SL",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_SL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of SerieA league of HALO SOKA service.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of SerieA league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT SL to 15602.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of SerieA league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT SL to 15602.\r\nEveryday, you will get the hot update of SerieA league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "HOT PL",
        "price" : NumberLong(0),
        "description" : "HOT_PL",
        "type" : "register",
        "package_code" : "HOT_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have registered the daily package of England Premier league of HALO SOKA service (Fee:100 Tsh) successfully.\r\nEveryday, you will get the  hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    },
    {
        "content" : "OFF HOT PL",
        "description" : "HOT_PL",
        "price" : NumberLong(0),
        "type" : "cancel",
        "package_code" : "HOT_PL",
        "mts_error" : {
            "ALREADY_EXIST" : "[HALO SOKA]\r\nYou have already registered the daily package of England Premier league of HALO SOKA service.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_ENOUGH_MONEY" : "[HALO SOKA]\r\nYou haven't enough balance to register the daily package of England Premier league of HALO SOKA service ((Fee:100 Tsh) or the subscriber is inactive.\r\nPlease refill your balance for registering the package.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "NOT_REGISTER_YET" : "[HALO SOKA]\r\nYou haven't yet registered the package.\r\nTo register the daily package, send HOT PL to 15602.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!",
            "FORWARD_PROCESS_FAIL" : ""
        },
        "mts_success" : [
            {
                "id" : NumberLong(1),
                "content" : "[HALO SOKA]\r\nYou have cancelled the daily package of England Premier league of HALO SOKA service successfully.\r\nTo register the daily package, send HOT PL to 15602.\r\nEveryday, you will get the hot update of England Premier league.\r\nFor detail information, please send HELP HOT to 15602 or call 100.\r\nThank you!"
            }
        ],
        "support_action_on_wap" : NumberLong(1),
        "use_check_mode" : NumberLong(0)
    }
],
    "mt_invalid_syntax" : "[HALO SOKA]\r\nWrong syntax!For detail information, please send HELP to 15602 or call 100.\r\nThank you!",
    "renew_use_check_mode" : NumberLong(0),
    "cp_ws_subscribe" : "http://10.58.37.176:8084/subscribe.php?wsdl",
    "cp_ws_monfee" : "http://10.58.37.176:8084/receiveresult.php?wsdl",
    "cp_ws_molistener" : "http://10.58.37.176:8084/molistener.php?wsdl",
    "cp_ws_getcontent" : "http://10.58.37.176:8084/getcontent.php?wsdl",
    "cp_ws_username" : "ttv",
    "cp_ws_password" : "ttv@livescore123",
    "cp_private_key" : "-----BEGIN PRIVATE KEY-----\r\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDoY0IfkeZDLI2U\r\nZvDQWJEAYt34boDdhC+W0OUcDc/bZgUsr1aXZ2p7WBbMrfmiDuTGg/KrfP/OsLKN\r\nhvJFnroHJltxDSCBac5IKr6yCKh9JzEBwbfHty+erKlRiME6TRs7lVqAwwYNmRYf\r\nv/5lsE4WvqF9djWDNL135WIFNS4qu9j2aEHz+aNuDFpwLbHYHs5KOC2Ix70iHhsN\r\nLDdFI8d0cIUzkpZMyiLKZxRFQHevufmAWEcSsYqO7K/N6+TEW7bBrDpWf+tDFChs\r\nBFHVPwirGMs9d1V/2SuKlqAJcKS8U76BPdHcnUaHK3oUwcGmlp2nTxRVk0Nl9Egv\r\naIbyrz17AgMBAAECggEBAIGVv+yczhsNQBHN6WE00f62IvMCo8dii6r855ViOlMs\r\nq5wEVjPtThpjv6r5JKfyyC3OrEZmWLsoGxkCdBzQawnLoNgEquifWJEzjMz4OQQ5\r\nE4HYRSQQCXTg8TXvHVjmYFY0bZg5qClzATxJMpH/4zuZp7Gvdh2pDjJKbgNbaVgY\r\nAaDEnf9gnPJWvIL0CfzRzPwcrqX7J9vZziuaDmAl/CAIYADA7t0KXiY/5VC853cy\r\nRPlqa8g6PUtctWXVWd+RKPqnn87KUMLvhiMF5f8IF5bFukqqEW2TbVhWLcJ1Zm7T\r\nTB8Pf0Pz6Ln6Aq9qDjetrJXGUyjL91MtI2NiAX4mP9kCgYEA/fS5rJPuvx+xK0Iu\r\ny3ecXD8zEh0B7XjpRJEZO78okTbsGKX5L/U8QL5mwKSla36CjIhuDe0z58KdKCkn\r\nUG8rJ0CI0m20Ju3aMSIaX50kPDDQRYBWIs6fhs/Dm6jaxb9+hezzxRpbBiGkg6Zt\r\nixZyIUADLk/7MJDYZnBs/bgJ6PUCgYEA6kIXf/0eqjMNfDWiZgKdfIQnYKeFLv/z\r\n0t4A2oOao18+P0AfqVavGYQBMsa0Y/jT3lcuVs0kjISNAD742m7ZV/2ZmyfOPiuS\r\nhDZ15nGzHpW/d8n5dSinzj5R1D20w54FcblbEBcPlETdS53Wz6O48UB2BwWX2TqU\r\nvHfroqueRq8CgYEA2ySYKLULG4AUmVimGsvfcLbUsje31x00Zgezt5vwPMchGu/2\r\nRQ0g7lmfP3OUj1r++pgsJ/ZdiQumI+bHVyBnI5sLtlIWWFV9qFJeYc2vXAVfY+on\r\n6idU/7c/e45NIsFpQ9ZCUP66N2NGnJg8BP7cg5bCZ+96ueDz7Ant808hthUCgYA1\r\nmUetNioVATddqxvsQT7tZYNo/wtfFJsiLT9+v8vbA4jHihF0emv355cqmgJeOWmo\r\nnsPwgmGRcSad3sqrl99M3a6OrI3o9alBbQCO6Ib2QYccjm7wWowCiYnJiRDaDUbr\r\nnWBzjwM2hocnrQagXbKMKJaYxrPWtSug78NPZfsB6wKBgQDlNCusHYGX5wxQVc6W\r\nItuE2uwXTlwcF/oTO71suv/c2EsGP/rUTP5A/DBuT7U10wFvYBs3mFGtdz1XE+Rn\r\nbCPmgO6DvnGs9BCZ3OHp8YXWlJkLokMEN0o9WMuNcrVR7jqj2Y+fSqMXwxmbTZQg\r\nvNMvjInnSHy4FvYiJfE9mKAwzQ==\r\n-----END PRIVATE KEY-----",
    "cp_public_key" : "-----BEGIN PUBLIC KEY-----\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6GNCH5HmQyyNlGbw0FiR\r\nAGLd+G6A3YQvltDlHA3P22YFLK9Wl2dqe1gWzK35og7kxoPyq3z/zrCyjYbyRZ66\r\nByZbcQ0ggWnOSCq+sgiofScxAcG3x7cvnqypUYjBOk0bO5VagMMGDZkWH7/+ZbBO\r\nFr6hfXY1gzS9d+ViBTUuKrvY9mhB8/mjbgxacC2x2B7OSjgtiMe9Ih4bDSw3RSPH\r\ndHCFM5KWTMoiymcURUB3r7n5gFhHErGKjuyvzevkxFu2waw6Vn/rQxQobARR1T8I\r\nqxjLPXdVf9kripagCXCkvFO+gT3R3J1Ghyt6FMHBppadp08UVZNDZfRIL2iG8q89\r\newIDAQAB\r\n-----END PUBLIC KEY-----",
    "redirect_wap" : "http://tuyengap.vn",
    "wap_provider_code" : "TTV",
    "wap_service_code" : "LIVESCORE",
    "wap_confirm_require" : NumberLong(1),
    "send_sms_ws_username" : "ttv",
    "send_sms_ws_password" : "i43DSA@#",
    "updated_at" : ISODate("2015-12-29T07:56:13.658Z")
}