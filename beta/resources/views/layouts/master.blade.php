@include('partials.header')

@include('partials.menu')

<section class="content">
    @if(Input::get('popup') != 'true')
        @include('partials.header_menu')
    @endif
            <!-- Header Ends -->

    @yield('main_content')
            <!-- Warper Ends Here (working area) -->
    <footer class="container-fluid footer">
        Copyright &copy; 2015 <a href="http://traithivang.vn/" target="_blank">Trai Thi Vang Jsc</a>
        <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
    </footer>
</section>

@include('partials.footer')