@include('partials.header')

<div class="container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <h3 class="text-center">Trai Thi Vang Jsc</h3>

            <p class="text-center">Please sign in first...</p>
            @if(Session::has('errors'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <strong>Error!</strong>
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif
            <hr class="clean">
            {!! Form::open(['url' => 'auth', 'role' => 'form']) !!}
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input name="email" type="email" class="form-control" placeholder="Email Adress">
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input name="password" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <label class="cr-styled">
                    <input type="checkbox">
                    <i class="fa"></i>
                </label>
                Remember me
            </div>
            <button type="submit" class="btn btn-purple btn-block">Sign in</button>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@include('partials.footer')