<div class="form-group">
    <label class="col-sm-2 control-label">{{$name}}</label>
    @if($input_type == 'select_Multiple')
        <div class="col-sm-9">
            <select class="form-control chosen-select" name="{{$value_name}}" multiple=""
                    data-placeholder="Multiple Select" required="">
                @if(isset($data))
                    @foreach($data as $items )
                        <option value="{{$items->id}}" style="padding: 10px 5px;">{{$items->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    @elseif($input_type == 'select_option')
        <div class="col-sm-9 select_option">
            <ul>
                @if(isset($data))
                    @foreach($data as $items )
                        @if(is_object($items))
                            <li><input type="checkbox" name="{{$value_name}}" value="{{$items->id}}">{{$items->name}}</li>
                        @else
                            <li><input type="checkbox" name="{{$value_name}}" value="{{$items}}">{{$items}}</li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    @elseif($input_type == 'text')
        <div class="col-sm-9">
            <input
                    @if($name == 'Title' ) value="@if(isset($data_title)){{$data_title}}@endif" required
                    @elseif($name == 'Slug') value="@if(isset($data_slug)){{$data_slug}}@endif"
                    @elseif($name == 'Meta title') value="@if(isset($data_meta_title)){{$data_meta_title}}@endif"
                    @elseif($name == 'Meta keywords') value="@if(isset($data_meta_keywords)){{$data_meta_keywords}}@endif"
                    @endif
                    class="form-control form-control-flat @if(isset($class)) {{$class}} @endif" type="text" name="{{$value_name}}"
            >
        </div>
    @elseif( $input_type == 'area' )
        <div class="col-sm-9">
    <textarea class="form-control form-control-flat"  name="{{$value_name}}"
              type="text" cols="30" data-rows="100"  @if($value_name == 'post_description')style="height: 150px"  @endif>@if(isset($data_description)){{$data_description}}@elseif(isset($data_meta_description)){{$data_meta_description}}@endif</textarea>
        </div>
    @elseif($input_type == 'area-edit')
        <div class="col-sm-9">
    <textarea class="form-control form-control-flat"  name="{{$value_name}}"
              @if($input_type == 'area-edit')id="edit" @endif type="text" cols="30" data-rows="100">@if(isset($data_content)){{$data_content}}@endif
</textarea>
        </div>
    @elseif($input_type == 'area-tags')
        <div class="col-sm-9">
            <textarea class="form-control form-control-flat " id="textarea" name="{{$value_name}}" rows="4"></textarea>
        </div>
    @elseif($input_type =='button')
        <div class="col-sm-9">
            <button class="btn btn-purple" id="">{{$button_name}}
            </button>
        </div>
    @endif
</div>