@extends('layouts.master')

@section('main_content')
    {!!Form::open(array('url' => URL::to('dashboard/post/edit-post')
    , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
    <div class="warper container-fluid new-posts">

        <div class="page-header">
            <h3>{{trans('content.zone_post')}}
            </h3>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-8 new-posts-left">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('content.post_content')}}</div>
                    <div class="panel-body">
                        <input type="hidden" name="post_id" value="{{ $post->_id }}">
                        @include('input.input' , ['name' => trans('content.post_title') , 'value_name' => 'post_name' , 'input_type' => 'text' , 'class' => 'title', 'data_title' => $post->post_name ])
                                <!--   @include('input.input' , ['name' => trans('content.post_slug') , 'value_name' => 'slug_url' , 'input_type' => 'text' , 'class' => 'slug', 'data_slug' => $post->slug_url ]) -->
                        <!--    <div class="form-group error_slug" style="display :none;">
                               <label class="col-sm-2 control-label"></label>
                               <div class="col-sm-9 alert-error">
                               </div>
                           </div> -->

                        @include('input.input' , ['name' => trans('content.post_description') , 'value_name' => 'post_description' , 'input_type' => 'area', 'data_description' => $post->post_description ])
                        @include('input.input' , ['name' => trans('content.post_content') , 'value_name' => 'post_content' , 'input_type' => 'area-edit', 'data_content' => $post->post_content ])
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('content.meta_seo')}}</div>
                    <div class="panel-body">
                        @include('input.input' , ['name' => trans('content.post_meta_title') , 'value_name' => 'meta_title' , 'input_type' => 'text' , 'data_meta_title' => $post->seo['title'] ])
                        @include('input.input' , ['name' => trans('content.post_meta_keywords') , 'value_name' => 'meta_keywords' , 'input_type' => 'text' , 'data_meta_keywords' => $post->seo['meta'] ])
                        @include('input.input' , ['name' => trans('content.post_meta_description') , 'value_name' => 'meta_description' , 'input_type' => 'area' , 'data_meta_description' => $post->seo['description'] ])
                        @include('input.input' , ['name' => trans('content.post_meta_tags') , 'value_name' => 'tags' , 'input_type' => 'area-tags' ])
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 new-posts-right">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('content.thumbnail_and_category')}}</div>
                    <div class="panel-body">
                        @include('partials.button-upload' ,  ['video_option' => false , 'image_option' => true ,  'data_post_thumbnail' => $post->post_thumbnail ])
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('content.post_language')}}</div>
                    <div class="panel-body">
                        <select name="lang_code" class="form-control">
                            <option <?php if($post->lang_code == trans('content.lang_vietnam') ) echo "selected"; ?> value="{{ trans('content.lang_vietnam') }}" >{{ trans('content.lang_vietnam') }}</option>
                            <option <?php if($post->lang_code == trans('content.lang_english') ) echo "selected"; ?> value="{{ trans('content.lang_english') }}">{{ trans('content.lang_english') }}</option>
                            <option <?php if($post->lang_code == trans('content.lang_lao')) echo "selected"; ?> value="{{ trans('content.lang_lao') }}">{{ trans('content.lang_lao') }}</option>
                        </select>
                    </div>
                </div>
                <div class="panel panel-default category">
                    <div class="panel-heading">{{trans('content.post_category')}}</div>
                    <div class="panel-body">
                        <table class="table table-hover mails">
                            <tbody>
                            <?php $categories = \App\Models\Category::all();?>
                            @if($categories)
                                @foreach($categories as $category)
                                    <tr>
                                        <td class="mail-select">
                                            <label class="cr-styled">
                                                <input
                                                        @if(isset($post->category_id))
                                                        @foreach($post->category_id as $category_post)
                                                        @if($category->id == $category_post)
                                                        checked
                                                        @endif
                                                        @endforeach  type="checkbox" name="category[]" value="{{$category->id}}" ng-model="todo.done" class="ng-pristine ng-untouched ng-valid" id="catid_{{$category->_id}}"
                                                        @endif
                                                >
                                                <i class="fa"></i>
                                            </label>
                                        </td>
                                        <td class="mail-content">
                                            <label for="catid_{{$category->_id}}">{{$category->name}}</label>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-8">
            <button type="submit" class="col-sm-2 btn btn-purple ">Submit</button>
        </div>
    </div>
    {!! Form::close() !!}
    @include('partials.upload')


@stop