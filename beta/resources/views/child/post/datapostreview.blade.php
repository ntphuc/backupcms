<div class="warper container-fluid new-posts">
    <div class="page-header">
        <h3>{{$post->post_name}}
        </h3>
        <h4>{{$post->post_description}}</h4>
        <h4>{!!$post->post_content!!}</h4>
    </div>
    <div class="col-md-12" align="center">
        <button  class="col-sm-2 btn btn-purple active_button" style="margin-left:-12px;" onclick="Active('{{$post->_id}}' , '{{$post->post_status}}')">@if($post->post_status == 0 ) Active @else Deactive @endif Post </button>
    </div>
 </div>
<script>
    function Active(id, status){
        $.ajax({
            method: "GET",
            url: "{{route('UpdateStatusPost')}}/" + id + '/' + status,
            success: function (data) {
                location.reload();
            }
        });
    }
</script>