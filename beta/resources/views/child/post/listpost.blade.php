@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header"><h3>Post Information</h3></div>
        <div class="row">
            <div class="col-md-12 list-post">
                <div class="panel panel-default">
                    <div class="panel-heading">Controls Table</div>
                    <div class="panel-body">
                        @if(session('success'))<span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <select class="form-control input-sm  form-control-flat" id="per_page">
                                        @if(!Session::has('post_per_page'))
                                            <option>Default Per Page</option>
                                        @endif
                                        <option  @if(Session::has('post_per_page') && Session::get('post_per_page') == '05') selected @endif  value="05">05</option>
                                        <option @if(Session::has('post_per_page') && Session::get('post_per_page') == '10') selected @endif value="10">10</option>
                                        <option @if(Session::has('post_per_page') && Session::get('post_per_page') == '20') selected @endif value="20">20</option>
                                        <option @if(Session::has('post_per_page') && Session::get('post_per_page') == '30') selected @endif value="30">30</option>
                                        <option  @if(Session::has('post_per_page') && Session::get('post_per_page') == '40') selected @endif value="40">40</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control input-sm  form-control-flat" id="media_by_user" name="user">
                                    <?php
                                    $user = App\Models\User::all();
                                    ?>
                                    <option @if(!Session::has('post_by_user')) selected="selected" @endif value="all">All</option>
                                    @foreach($user as $value)
                                        <option value="{{$value->id}}"
                                                @if(Session::get('post_by_user') == $value->id) selected @endif>{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3 col-lg-offset-5">
                                {!!Form::open(array('url' => route('post')
                                , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input id="search_text" type="text" class="form-control input-sm  form-control-flat "
                                           placeholder="Search record here..." name="search">
                                    <span class="input-group-btn">
                                        <button id="search_go" class="btn btn-default btn-sm btn-flat " type="submit">Go!</button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!!Form::open(array('url' => route('post.ActionPost')
                        , 'class' => 'form-inline' , 'role' => 'form ' , 'method' => 'POST'))!!}
                        <div class="row" style="margin-bottom: 10px;    ">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select class="form-control  form-control-flat input-sm" name="select_action">
                                        <option value="Actions">Actions</option>
                                        <option value="Delete">Delete</option>
                                        <option value="Active">Active</option>
                                        <option value="Deactive">Deactive</option>
                                    </select>
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Apply</button>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="1">
                                    <label class="cr-styled">
                                        <input type="checkbox" id="check_all">
                                        <i class="fa"></i>
                                    </label>
                                </th>
                                <th class="col-sm-2">Avatar</th>
                                <th class="col-sm-2">Name</th>
                                <th class="col-sm-2">Create By</th>
                                <th class="col-sm-2">Created At</th>
                                <th class="col-sm-1">Status</th>
                                <th class="col-sm-1">Review</th>
                                <th class="col-sm-1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <?php
                                $user_create = App\Models\User::find($post->user_id);
                                ?>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" ng-model="todo.done" name="listId[]"
                                                   value="{!! $post->_id !!}">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>
                                        <img src="{{$post->post_thumbnail}}" style="width:100%;">
                                    </td>
                                    <td style="text-align: left">{{$post->post_name}}</td>
                                    @if(isset($user_create))
                                        <td width="10">{{$user_create->name}}</td>
                                    @else
                                        <td width="10">None</td>
                                    @endif
                                    <td>{{date('D d/m/Y' ,strtotime($post->created_at))}}</td>
                                    <td>
                                        @if($post->post_status == 1)
                                            <i class="fa fa-check" style="color:#428BCA "></i>
                                        @elseif($post->post_status == 0)
                                            <i class="fa fa-close" style="color:#2b2f3e "></i>
                                        @endif
                                    </td>
                                    <td width="1">
                                        <i data-toggle="modal" style="cursor: pointer"
                                                     class="glyphicon glyphicon-play-circle" data-video=""
                                                     onclick="Review('{{ $post->_id}}')">
                                        </i>
                                    </td>
                                    <td>
                                        <select style="width:100%;" class="form-control input-sm form-control-flat select-action" post_id="{{$post->id}}" id="per_page">
                                            <option style="width:100%;" disabled selected="">Action</option>
                                            <option style="width:100%;"  value="edit">Edit</option>
                                            <option style="width:100%;" value="delete">Delete</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! Form::close() !!}
                        <div class="row">
                            <div class="col-lg-9">
                                {{--<div class="form-group">--}}
                                {{--<form class="form-inline" role="form">--}}
                                {{--<select class="form-control  form-control-flat input-sm">--}}
                                {{--<option>Actions</option>--}}
                                {{--<option>Delete</option>--}}
                                {{--<option>Merge</option>--}}
                                {{--<option>Edit</option>--}}
                                {{--</select>--}}
                                {{--<button class="btn btn-default btn-sm btn-flat" type="button">Apply</button>--}}
                                {{--</form>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-lg-3">
                                {!!Form::open(array('url' => route('post')
                                , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm  form-control-flat"
                                           placeholder="Search record here..." name="search">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Go!</button>
                                </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!! $posts->appends(Input::except('page'))->render() !!}

                    </div>
                </div>
            </div>
            @include('popup.popup_reviewpost')
        </div>
    </div>
        <script>
            $('#per_page').on('change', function () {
                $value = $(this).val();
                $.ajax({
                    method: "POST",
                    url: "{{route('post.ActionPost')}}",
                    data: {session: $value, _token: "{{csrf_token()}}"},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
            $('#media_by_user').on('change', function () {
                $value = $(this).val();
                $.ajax({
                    method: "POST",
                    url: "{{route('post.ActionPost')}}",
                    data: {user: $value, _token: "{{csrf_token()}}"},
                    success: function (data) {
                        location.reload();
                    }
                });

            });
            $('.close_reload').on('click', function () {
                location.reload();
            });
            function Review(id) {
                $.ajax({
                    url: '{{ URL::to('dashboard/post/data-post-review') }}',
                    data: {post_id: id, _token: "{{csrf_token()}}"},
                    type: 'GET',
                    success: function (resp) {
                        $('.review-post .box-review-post').html(resp);
                        $('#review-post .post_id_publish').val(id);
                        $('#button_review_post').click();
                    },
                    error: function(resp){
                        alert('error!');
                    }
                });
            }

            $('.list-post .select-action').change(function(){
                if($(this).val() == 'edit') {
                    $('.list-post .select-action').prop('selectedIndex',0);
                    var url = "{{route('post.editPost')}}/" + $(this).attr('post_id');
                    window.location = url;
                    window.location.replace (url);
                }
                if($(this).val() == 'delete'){
                    $('.list-post .select-action').prop('selectedIndex',0);
                    if(confirm('Are you sure delete it?')) {
                        $.ajax({
                            url: '{{ route('post.ActionPost') }}',
                            data: { listId: [$(this).attr('post_id')] , select_action : 'Delete', type : 'ajax', _token: "{{csrf_token()}}"},
                            type: 'POST',
                            success: function(resp) {
                                if(resp.status) {
                                    location.reload();
                                } else {
                                    alert('Delete error');
                                }
                            },
                            error: function(resp) {
                                alert('Erorr!');
                            }
                        });
                    }
                }
            });

        </script>

        <script type="text/javascript">
            $("#check_all").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        </script>
@stop