<?php
return [
    'meta_seo' => 'SEO Google & Facebook' ,
    'thumbnail_and_category' => 'Thumbnail & Category',
    'add_media_to_content' => 'Add Media To Content' ,
    'zone_post' => 'Zone Of Post' ,
    'post_content' => 'Post Content' ,
    'upload_image' => 'Upload Image',
    'title' => 'Title' ,
    'meta_title' => 'Meta Title' ,
    'meta_description' => 'Meta Description' ,
    'meta_keywords' => 'Meta Keywords' ,
    'meta_tags' => 'Meta Tags' ,
    'post_url' => 'Slug Post' ,
    'media_description' => 'Media Description' ,
    'language' => 'Language' ,
    'description' => 'Description' ,
    'tags' => 'Tags' ,
    'upload_video' => 'Upload Video' ,
    'name_media' => 'Name Media' ,
    'upload_media' => 'Upload Media',
    'category' => 'Category',
    'post_title' => 'Title',
    'post_slug' => 'Slug',
    'post_language' => 'Language',
    'post_description'  => 'Description',
    'post_meta_title'  => 'Meta title',
    'post_meta_keywords'  => 'Meta keywords',
    'post_meta_description'  => 'Meta description',
    'post_meta_tags'  => 'Meta tags',
    'post_category' => 'Category',
    'lang_vietnam'  => 'Việt Nam',
    'lang_english'  => 'English',
    'lang_lao'  => 'Lào',
    'timeline' => 'Timeline'
] ;