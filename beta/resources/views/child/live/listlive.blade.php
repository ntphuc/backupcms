@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header"><h3>{{ trans('content.live_information') }}</h3></div>
        <div class="row">
            <div class="col-md-2" style="margin-bottom:10px;">
                <a class="btn btn-success btn-block " href="{{ URL::to('dashboard/live/create') }}">
                    {{ trans('content.new_live') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 list_live">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('content.list_live') }}</div>
                    <div class="panel-body">
                    @include('partials.notify')
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <select class="form-control input-sm  form-control-flat" id="per_page">
                                        @if(!Session::has('live_per_page'))
                                            <option selected>Default Per Page</option>
                                        @endif
                                        <option value="5" @if(Session::has('live_per_page') && Session::get('live_per_page') == '05') selected @endif>05</option>
                                        <option value="10" @if(Session::has('live_per_page') && Session::get('live_per_page') == '10') selected @endif>10</option>
                                        <option value="20" @if(Session::has('live_per_page') && Session::get('live_per_page') == '20') selected @endif>20</option>
                                        <option value="30" @if(Session::has('live_per_page') && Session::get('live_per_page') == '30') selected @endif>30</option>
                                        <option value="40" @if(Session::has('live_per_page') && Session::get('live_per_page') == '40') selected @endif>40</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3 col-lg-offset-5">
                                {!!Form::open(array('url' => route('live.ListLive')
                                        , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm  form-control-flat"
                                           placeholder="Search record here..." name="q">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm btn-flat" type="submit">Go!</button>
                                      </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!!Form::open(array('url' => route('live.ActionLive')
                                        , 'class' => 'form-inline' , 'role' => 'form ' , 'method' => 'POST'))!!}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select name="select_action" class="form-control  form-control-flat input-sm">
                                        <option value="">Actions</option>
                                        <option value="live">Live</option>
                                        <option value="dislive">Dislive</option>
                                        <option value="Delete">Delete</option>
                                    </select>
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Apply</button>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="1">
                                    <label class="cr-styled">
                                        <input type="checkbox" id="check_all">
                                        <i class="fa"></i>
                                    </label>
                                </th>
                                <th width="1">Name</th>
                                <th width="1">Score</th>
                                <th width="1">Time Match</th>
                                <th width="1">Status</th>
                                <th width="1">Event</th>
                                <th width="1">Review</th>
                                <th width="1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($live_datas as $live)
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" name="listId[]" value="{{$live->id}}" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>{{$live->homeTeamName}} vs {{$live->awayTeamName}}</td>
                                    <td> {{$live->result['goalsHomeTeam']}} - {{$live->result['goalsAwayTeam']}}</td>
                                    <td>@if($live->date) {{date('H:i d/m/Y' ,$live->date->sec)}} @endif</td>
                                    <td>@if($live->have_report == true) Live @else No @endif</td>
                                    <td width="1">
                                        <a href="{{URL::to('/dashboard/live/event')}}/{{$live->_id}}">
                                            <i data-toggle="modal" style="cursor: pointer" class="glyphicon glyphicon-play-circle" data-video="" ></i>
                                        </a>
                                    </td>
                                    <td width="1">
                                        <i data-toggle="modal" style="cursor: pointer" class="glyphicon glyphicon-play-circle" data-video="" onclick='reviewLive("{{$live->id}}")' >
                                        </i>
                                    </td>
                                    <td>
                                        <select class="form-control input-sm form-control-flat select-action" live_id="{{$live->id}}" id="per_page">
                                            <option selected="">Action</option>
                                            <option value="edit">Edit</option>
                                            <option value="delete">Delete</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-group">
                                    <select name="select_action_2" class="form-control  form-control-flat input-sm">
                                        <option value="">Actions</option>
                                        <option value="live">Live</option>
                                        <option value="dislive">Dislive</option>
                                        <option value="Delete">Delete</option>
                                    </select>
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Apply</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="col-lg-3 form-search-2">
                                {!!Form::open(array('url' => route('live.ListLive')
                                        , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm  form-control-flat"
                                           placeholder="Search record here..." name="q">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm btn-flat" type="submit">Go!</button>
                                      </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!! $live_datas->render() !!}

                    </div>
                </div>
            </div>
            @include('popup.popup_editlive')
            @include('popup.popup_reviewlive')

        </div>


    </div>

    <script>
        $('#per_page').on('change', function () {
            $value = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{route('live.ActionLive')}}",
                data: {live_per_page: $value, _token: "{{csrf_token()}}"},
                success: function (data) {
                    location.reload();
                }
            });
        });
        $('.close_reload').on('click', function () {
            location.reload();search
        });
        function reviewLive(id) {
            $.ajax({
                url: '{{ route('live.getTableReviewLive') }}',
                data: {live_id: id, _token: "{{csrf_token()}}"},
                type: 'GET',
                success: function (resp) {
                    $('.button_review_live .box-reviewlive').html(resp);
                    $('#button_review_live .live_id_publish').val(id);
                    $('.button_review_live').click();
                },
                error: function(resp){
                    alert('error!');
                }
            });
        }

        // check live/no
        var goalsHomeTeam = goalsAwayTeam = 0;
        $('#editLive .live_live').change(function(){
            if($('#editLive .box-result').attr('status') == 'off') {
                $('#editLive .box-result').attr('status', 'on');
                $('#editLive .box-result').html('<div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-home" value="'+goalsHomeTeam+'"></div> <div class="col-xs-2">-</div> <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-away" value="'+goalsAwayTeam+'"></div>');
            } else {
                $('#editLive .box-result').attr('status', 'off');
                $('#editLive .box-result').html('<div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-home" value="'+goalsHomeTeam+'" readonly></div> <div class="col-xs-2">-</div> <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" value="'+goalsAwayTeam+'" name="result-away" readonly></div>');
            }
        });

        $('.list_live .select-action').change(function(){
            if($(this).val() == 'edit') {
                $('.list_live .select-action').prop('selectedIndex',0);
                $('.open-popup-edit').click();
                $.ajax({
                    url: '{{ route('live.getEdit') }}',
                    data: { live_id : $(this).attr('live_id') , _token : "{{csrf_token()}}"},
                    type: 'GET',
                    success: function(resp) {
                        resp = JSON.parse(resp);
                        goalsHomeTeam = resp.result['goalsHomeTeam'];
                        goalsAwayTeam = resp.result['goalsAwayTeam'];
                        $('#editLive .live_id').attr('value', resp._id);
                        $('#editLive .homeTeamName').val(resp.homeTeamName);
                        $('#editLive .homeTeamLogo').val(resp.homeTeamLogo);
                        $('#editLive .awayTeamName').val(resp.awayTeamName);
                        $('#editLive .awayTeamLogo').val(resp.awayTeamLogo);
                        $('#editLive .league').val(resp.league);
                        $('#editLive .matchday').val(resp.matchday);
                        $('#editLive .result-home').val(resp.result['goalsHomeTeam']);
                        $('#editLive .result-away').val(resp.result['goalsAwayTeam']);
                        $('#editLive .date').val(resp.date);
                        $('#editLive .fr-box.fr-basic .fr-element').html(resp.description);  // Set description
                        $('#editLive .edit-content-event').val(resp.description);
                        $('#editLive .live_id').val(live_id);
                        if(resp.have_report == true && $('#editLive .have_report').attr('checked') != 'checked') {
                            $('#editLive .have_report').attr('checked', 'checked');
                        } else {
                            $('#editLive .have_report').removeAttributeNS('checked');
                        }
                        $('.open-popup-edit').click();
                    },
                    error: function(resp) {
                        alert('show data edit error!');
                    }
                });
            } else if($(this).val() == 'delete') {
                $('.list_live .select-action').prop('selectedIndex',0);
                if(confirm('Are you sure delete it?')) {
                    $.ajax({
                        url: '{{ route('live.ActionLive') }}',
                        data: { listId: [$(this).attr('live_id')] , select_action : 'Delete', type : 'ajax', _token: "{{csrf_token()}}"},
                        type: 'POST',
                        success: function(resp) {
                            if(resp.status) {
                                location.reload();
                            } else {
                                alert('Delete error');
                            }
                        },
                        error: function(resp) {
                            alert('Erorr!');
                        }
                    });
                }
            }
        });
    </script>
@stop         