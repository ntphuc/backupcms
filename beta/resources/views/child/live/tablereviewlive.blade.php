<?php
//  define color pannel
$icon_color = array(
        'text'      => 'bg-purple',
        'goal'      => 'bg-danger',
        'og_goal'   =>'bg-warning',
        'pen'       => 'bg-info',
        'miss_pen'  => 'bg-success',
        'yellow_card'=> 'bg-info',
        'fulltime'  => 'bg-danger',
);

$pannel_color = array(
        'text'      => 'panel-primary',
        'goal'      => 'panel-danger',
        'og_goal'   => 'panel-warning',
        'pen'       => 'panel-info',
        'miss_pen'  => 'panel-success',
        'yellow_card'=> 'panel-info',
        'fulltime'  => 'panel-danger',
);
//  define event important
$event_important = array('goal', 'pen', 'miss_pen', 'yellow_card', '2yellow_card');
//  define event orther
$event_orther = array('text', 'fulltime');
?>
@if($live->events)
    <div class="col-xs-12 table-review-live"> {{--Su kien noi bat--}}
        <table class="table table-bordered">
            <tbody>
            @foreach($live->events as $key => $item)
                @if(in_array($item['type'], $event_important) && isset($item['player']))
                    <tr class="active">
                        @if(!empty($key)) <input type="hidden" name="event_key" class="event_key" value="{{$key}}"> @endif
                        <td  style="background-color : transparent;" class="col-xs-1"><label>{{$item['minutes']}}'</label></td>
                        @if($item['event_of_team'] == 'home')
                            <td style="background-color : transparent;border-right: none;" class="col-xs-4" ><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }}</label></td>
                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-transparent"></i></td>
                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"></td>
                        @else
                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"></td>
                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-transparent"></i></td>
                            
                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                            <td class="col-xs-4" style="background-color : transparent;transparent;border-left: none;"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }}</label></td>
                        @endif
                    </tr>
                @elseif($item['type'] == 'og_goal' && isset($item['player']))
                    <tr class="active">
                        @if(!empty($key)) <input type="hidden" name="event_key" class="event_key" value="{{$key}}"> @endif
                        <td  style="background-color : transparent;" class="col-xs-1"><label>{{$item['minutes']}}'</label></td>
                        @if($item['event_of_team'] == 'home')
                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"></td>
                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-transparent"></i></td>
                            
                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }} <font color="red">OG</font></label></td>
                        @else
                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }} <font color="red">OG</font></label> </td>
                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                            
                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-transparent"></i></td>
                            <td class="col-xs-4" style="background-color : transparent;border-left: none;"></td>
                        @endif
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
@endif