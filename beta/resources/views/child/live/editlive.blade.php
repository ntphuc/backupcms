@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3 style="margin-left:3%;">{{ trans('content.edit_live') }}
                <small>Edit live / detailed information</small>
            </h3>
        </div>
        @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
        {!!Form::open(array('url' => URL::to('dashboard/live/edit')
                                        , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
        <div class="col-md-12 popup-editLive">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.event_home_team') }}</div>
                    <div class="panel-body">
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.event_home_team') }}</label>
                            <div class="col-xs-10">
                                <input type="hidden" name="live_id" value="{{$live->id}}">
                                <input class="form-control homeTeamName" type="text" name="homeTeamName" value="" required>
                            </div>
                        </div>
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.logo_home') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="homeTeamLogo" value="" required>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.event_away_team') }}</div>
                    <div class="panel-body">
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.event_away_team') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="awayTeamName" value="" required>
                            </div>
                        </div>
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.logo_away') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="awayTeamLogo" value="" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.live_description') }}</div>
                    <div class="panel-body">
                        <textarea class="form-control form-control-flat " name="description" id="edit" type="text" cols="50"
                                  data-rows="100"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group home-team">
                <label>{{ trans('content.live_live') }}</label>
                <div class="switch-button sm showcase-switch-button">
                    <input id="switch-button-1" type="checkbox" name="have_report" value="true">
                    <label for="switch-button-1"></label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-purple">Submit</button>hung
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
    @include('partials.upload')
@stop
