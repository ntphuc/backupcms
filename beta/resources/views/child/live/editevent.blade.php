@extends('layouts.master')

@section('main_content')
    <style type="text/css">
        aside.left-panel {
            display: none;
        }

        .content {
            margin-left: 0;
        }
    </style>

    <div class="warper container-fluid create-event">

        <div class="page-header">
            <h1>{{ trans('content.edit_event') }}</h1>
        </div>

        <div class="row">
            {!!Form::open(array('url' => URL::to('dashboard/live/edit-event')
                                            , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
            <div class="col-sm-12 create-live">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('content.edit_event') }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Type</label>
                            <select name="type" class="form-control chosen-select" data-placeholder="Choose a Country">
                                <option value="text" @if($event['type'] == 'text') selected @endif>Text</option>
                                <option value="goal" @if($event['type'] == 'goal') selected @endif>Goal</option>
                                <option value="og_goal" @if($event['type'] == 'og_goal') selected @endif>og_goal</option>
                                <option value="pen" @if($event['type'] == 'pen') selected @endif>pen</option>
                                <option value="miss_pen" @if($event['type'] == 'miss_pen') selected @endif>miss_pen</option>
                                <option value="red_card" @if($event['type'] == 'red_card') selected @endif>Red Card</option>
                                <option value="yellow_card" @if($event['type'] == 'yellow_card') selected @endif>Yellow Card</option>
                            </select>

                        </div>
                        <hr class="dotted">
                        <div class="form-group home-team">
                            <label>{{ trans('content.event_home_team') }}</label>
                            <div class="switch-button showcase-switch-button">
                                <input id="switch-button-6" name="event_of_team" type="radio" value="home" @if($event['event_of_team'] == 'home') checked @endif>
                                <label for="switch-button-6"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{ trans('content.event_away_team') }}</label>
                            <div class="switch-button showcase-switch-button">
                                <input id="switch-button-7" name="event_of_team" type="radio" value="away" @if($event['event_of_team'] == 'away') checked @endif>
                                <label for="switch-button-7"></label>
                            </div>
                        </div>
                        <hr class="dotted">
                        <div class="form-group">
                            <label>{{ trans('content.event_player') }}</label>
                            <input type="text" name="player" class="form-control player" value="@if(isset($event['player'])){{$event['player']}}@endif" placeholder="Ronando">
                            <input type="hidden" name="live_id" class="form-control" value="{{$live_id}}">
                            <input type="hidden" name="event_key" class="form-control" value="{{$event_key}}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('content.event_time') }}</label>
                            <input type="number" name="minutes" class="form-control" value="@if(isset($event['minutes'])){!! (int) $event['minutes'] !!}@endif" placeholder="00:00" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('content.event_content_event') }}</div>
                    <div class="panel-body">
                     <textarea class="form-control form-control-flat " name="content" id="edit" type="text" cols="50"
                               data-rows="100">@if(isset($event['content'])) {{$event['content']}} @endif</textarea>
                    </div>
                </div>

            </div>
            <div class="col-xs-12">
                <button class="btn btn-success btn-block" type="submit">Edit</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
@stop