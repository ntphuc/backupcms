@extends('layouts.master')

@section('main_content')

<?php
//  define color pannel
$icon_color = array(
        'text'      => 'bg-purple',
        'goal'      => 'bg-danger',
        'og_goal'   =>'bg-warning',
        'pen'       => 'bg-info',
        'miss_pen'  => 'bg-success',
        'yellow_card'=> 'bg-info',
        'fulltime'  => 'bg-danger',
);

$pannel_color = array(
        'text'      => 'panel-primary',
        'goal'      => 'panel-danger',
        'og_goal'   => 'panel-warning',
        'pen'       => 'panel-info',
        'miss_pen'  => 'panel-success',
        'yellow_card'=> 'panel-info',
        'fulltime'  => 'panel-danger',
);

//  define event important
$event_important = array('goal', 'pen', 'miss_pen', 'yellow_card', '2yellow_card');
//  define event orther
$event_orther = array('text', 'fulltime');
?>
    {!!Form::open(array('url' => URL::to('dashboard/live/event')
                                            , 'class' => 'form-horizontal' , 'role' => 'form '))!!}

    <div class="warper container-fluid create-event">

        <div class="page-header">
            <h1>{{trans('content.timeline')}}
                <small>{{ trans('content.view_your_events') }}</small>
            </h1>
        </div>

        @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
        <div class="row">

            <div class="col-sm-3 box-create-event">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('content.add_new_events') }}</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label>Type</label>
                            <select name="type" class="form-control chosen-select" data-placeholder="Choose a Country">
                                <option value="text">Text</option>
                                <option value="goal">Goal</option>
                                <option value="og_goal">og_goal</option>
                                <option value="pen">pen</option>
                                <option value="miss_pen">miss_pen</option>
                                <option value="red_card">Red Card</option>
                                <option value="yellow_card">Yellow Card</option>
                                <option value="2yellow_card">2 Yellow Card</option>
                                <option value="fulltime">Finished</option>
                            </select>
                        </div>
                        <hr class="dotted">
                        <div class="form-group home-team">
                            <label>{{ trans('content.event_home_team') }}</label>
                            <div class="switch-button showcase-switch-button">
                                <input id="switch-button-6" name="event_of_team" type="radio" value="home" checked>
                                <label for="switch-button-6"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{ trans('content.event_away_team') }}</label>
                            <div class="switch-button showcase-switch-button">
                                <input id="switch-button-7" name="event_of_team" type="radio" value="away">
                                <label for="switch-button-7"></label>
                            </div>
                        </div>
                        <hr class="dotted">
                        <div class="event_player"></div>
                        <div class="form-group">
                            <label>{{ trans('content.event_time') }}</label>
                            <input type="number" name="minutes" class="form-control minutes" placeholder="30" required>
                            <input type="hidden" name="live_id" class="form-control live_id" value="{{$live->id}}" required>
                        </div>
                        <hr class="dotted">
                        <div class="form-group no-margn">
                            <button class="btn btn-success btn-block" type="button" data-toggle="modal"
                                    data-target="#timeline-event">{{ trans('content.add_new_event') }}
                            </button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="col-xs-12 logo">
                    <div class="col-md-6 col-md-push-3 box">
                        <div class="col-xs-5 logo-left">
                            <img src="{{env('link_static') . str_replace(env('folder_remove') , '' , $live->homeTeamLogo )}}">
                        </div>
                        <div class="col-xs-2 vs">
                            <img src="http://125.212.193.90:2016///img/56c2f7a9e628e.png">
                        </div>
                        <div class="col-xs-5 logo-right">
                            <img src="{{env('link_static') . str_replace(env('folder_remove') , '' , $live->awayTeamLogo )}}">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 result">           {{--Ti so--}}
                    <div class="col-md-6 col-md-push-3 box">
                        <span class="ti-so"><h3 class="text-purple">@if(!empty($live->result)) {!! $live->result['goalsHomeTeam'] !!} - {!! $live->result['goalsAwayTeam'] !!} @else 0 - 0 @endif</h3></span>
                    </div>
                </div>
                @if($live->events)
                    <div class="col-xs-12 event-important"> {{--Su kien noi bat--}}
                        <table class="table no-margn">
                            <tbody>
                            @foreach($live->events as $key => $item)
                                @if(in_array($item['type'], $event_important) && isset($item['player']))
                                    <tr class="active">
                                        @if(!empty($key)) <input type="hidden" name="event_key" class="event_key" value="{{$key}}"> @endif
                                        <td class="col-xs-1"><label>{{$item['minutes']}}'</label></td>
                                        @if($item['event_of_team'] == 'home')
                                            <td class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }}</label></td>
                                            <td class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                                            <td class="col-xs-1">|</td>
                                            <td class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td class="col-xs-4"></td>
                                        @else
                                            <td class="col-xs-4"></td>
                                            <td class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td class="col-xs-1">|</td>
                                            <td class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                                            <td class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }}</label></td>
                                        @endif
                                        <td><i class="fa fa-close delete-event" onclick='deleteEvent( "{{$live->id}}" , "{{$key}}" )'></i></td>
                                    </tr>
                                @elseif($item['type'] == 'og_goal' && isset($item['player']))
                                    <tr class="active">
                                        @if(!empty($key)) <input type="hidden" name="event_key" class="event_key" value="{{$key}}"> @endif
                                        <td class="col-xs-1"><label>{{$item['minutes']}}'</label></td>
                                        @if($item['event_of_team'] == 'home')
                                            <td class="col-xs-4"></td>
                                            <td class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td class="col-xs-1">|</td>
                                            <td class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                                            <td class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }} <font color="red">OG</font></label></td>
                                        @else
                                            <td class="col-xs-4"><label>{{ str_replace( '</p>', '',str_replace( '<p>', '', $item['player'] )) }} <font color="red">OG</font></label> </td>
                                            <td class="col-xs-1 icon-event"><i class="icon-{{$item['type']}}"></i></td>
                                            <td class="col-xs-1">|</td>
                                            <td class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td class="col-xs-4"></td>
                                        @endif
                                        <td class="col-xs-1"><i class="fa fa-close delete-event" onclick='deleteEvent( "{{$live->id}}" , "{{$key}}" )'></i></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <ul class="timeline list-unstyled">         {{--Danh sach binh luan--}}
                        @foreach($live->events as $key => $item)
                            @if(in_array($item['type'], $event_orther) && $item['content'] != '')
                                <li class="clearfix">
                                    @if(!empty($key)) <input type="hidden" name="event_key" class="event_key" value="{{$key}}"> @endif
                                    <time class="tl-time">
                                        <h3 class="text-purple">@if(!empty($item['minutes'])) {{$item['minutes']}} @endif</h3>
                                        <p>@if(!empty($item['event_of_team']) && $item['type'] != 'fulltime') {{$item['event_of_team']}} @else Finished @endif</p>
                                    </time>
                                    <i class="fa fa-comments-o {{$icon_color[$item['type']]}} tl-icon text-white btn btn-success btn-block " type="button" data-toggle="modal" data-target="#editEvent"></i>
                                    <div class="tl-content">
                                        <div class="panel {{$pannel_color[$item['type']]}}">
                                            <div class="panel-heading">{{$item['type']}}<i class="fa fa-close delete-event" onclick='deleteEvent( "{{$live->id}}" , "{{$key}}" )'></i></div>
                                            <div class="panel-body">
                                                <?php print $item['content']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>

        </div>
    </div>

    <button style="display:none;" id="open-box-edit-event" type="button" data-toggle="modal" data-target="#editEvent"></button>
    <div class="modal fade" id="timeline-event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('content.event_content_event') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-sm-12">
                                    <textarea class="form-control form-control-flat " name="content" id="edit" type="text" cols="50"
                                              data-rows="100"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog popup-edit-event">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close_reload" data-dismiss="modal"><span
                                aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Event</h4>
                </div>
                <div class="modal-body">

                    {!!Form::open(array('url' => URL::to('dashboard/live/edit-event')
                                                , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                    <div class="col-sm-12 box-create-event">
                        <div class="panel panel-default">
                            <div class="panel-heading">{{ trans('content.edit_event') }}</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select name="type" class="form-control chosen-select" data-placeholder="Choose a Country">
                                        <option value="text" data="text">Text</option>
                                        <option value="goal" data="goal">Goal</option>
                                        <option value="og_goal" data="og_goal">og_goal</option>
                                        <option value="pen" data="pen">pen</option>
                                        <option value="miss_pen" data="miss_pen">miss_pen</option>
                                        <option value="red_card" data="red_card">Red Card</option>
                                        <option value="yellow_card" data="yellow_card">Yellow Card</option>
                                        <option value="fulltime" data="fulltime">Finished</option>
                                    </select>

                                </div>
                                <hr class="dotted">
                                <div class="form-group home-team">
                                    <label>{{ trans('content.event_home_team') }}</label>
                                    <div class="switch-button showcase-switch-button">
                                        <input id="switch-button-8" class="event_of_team1" name="event_of_team" type="radio" value="home" >
                                        <label for="switch-button-8"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('content.event_away_team') }}</label>
                                    <div class="switch-button showcase-switch-button">
                                        <input id="switch-button-9" class="event_of_team2" name="event_of_team" type="radio" value="away" >
                                        <label for="switch-button-9"></label>
                                    </div>
                                </div>
                                <hr class="dotted">
                                <div class="event_player"></div>
                                <div class="form-group">
                                    <label>{{ trans('content.event_time') }}</label>
                                    <input type="number" name="minutes" class="form-control minutes" value="" placeholder="00:00" required>
                                    <input type="hidden" name="event_key" class="form-control event_key" value="" required>
                                    <input type="hidden" name="live_id" class="form-control live_id" value="{{$live->id}}" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">{{ trans('content.event_content_event') }}</div>
                            <div class="panel-body">
                     <textarea class="form-control form-control-flat edit edit-content-event" name="content" type="text" cols="50"
                               data-rows="100"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-success btn-block" type="submit">Edit</button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">     // Show/Hidden player
        $('.chosen-select').each(function(){
            $(this).change(function(){
                var panel = $(this).parents('.panel:first');
                var event_player = panel.find('.event_player');
                if($(this).val() != 'text' && $(this).val() != 'fulltime') {
                    if(event_player.html() == '') {
                        event_player.html('<div class="form-group player"><label>{{ trans("content.event_player") }}</label> <input type="text" name="player" class="form-control player" value="" placeholder="Ronando" required></div><hr class="dotted">');
                    }
                } else {
                    event_player.html('');
                }
            });
        })
    </script>

    {{--Edit Event: Truyen bien vao popup edit--}}
    <script type="text/javascript">
        function setDataPopupEditEvent(resp, event_key) {
            $('.popup-edit-event .chosen-select option').each(function(){     // Set type
                if($(this).attr('data') == resp.type) {
                    $(this).attr('selected', 'selected');
                }
            });

            if($('.popup-edit-event .event_of_team1').val() == resp.event_of_team) {           // Set event_of_team
                $('.popup-edit-event .event_of_team2').removeAttr('checked');
                $('.popup-edit-event .event_of_team1').attr('checked', 'checked');
            } else {
                $('.popup-edit-event .event_of_team1').removeAttr('checked');
                $('.popup-edit-event .event_of_team2').attr('checked', 'checked');
            }

            $('.popup-edit-event .event_key').val(event_key);  // Set event_key
            $('.popup-edit-event .minutes').val(resp.minutes);              // Set minutes
            $('.popup-edit-event .fr-box.fr-basic .fr-element').html(resp.content);  // Set content
            $('.popup-edit-event .edit-content-event').val(resp.content);
        }

        $('.create-event .timeline li > i').each(function(){  // Neu click vao timeline
            $(this).click(function(){
                var li = $(this).parents('li:first');
                var event_key = li.find('input.event_key').val();
                var live_id = $('.live_id').val();
                $.ajax({
                    url: '{{ URL::to('dashboard/live/edit-event') }}',
                    data: { live_id : live_id , event_key : event_key , _token : "{{csrf_token()}}"},
                    type: 'GET',
                    success: function(resp) {
                        resp = JSON.parse(resp);
                        setDataPopupEditEvent(resp, event_key);
                    },
                    error: function(resp) {
                        alert('show popup edit error!');
                    }
                });
            });
        });

        $('.create-event table tbody tr td label').each(function(){  // Neu click vao table
            $(this).click(function(){
                var tr = $(this).parents('tr:first');
                event_key = tr.find('input.event_key').val();
                var live_id = $('.live_id').val();
                $.ajax({
                    url: '{{ URL::to('dashboard/live/edit-event') }}',
                    data: { live_id : live_id , event_key : event_key, _token : "{{csrf_token()}}"},
                    type: 'GET',
                    success: function(resp) {
                        resp = JSON.parse(resp);
                        $('#editEvent .event_player').html('<div class="form-group player"><label>{{ trans("content.event_player") }}</label><input type="text" name="player" class="form-control player" value="'+resp.player+'" placeholder="Ronando" required></div>');
                        setDataPopupEditEvent(resp, event_key);
                    },
                    error: function(resp) {
                        alert('show popup edit error!');
                    }
                });
                $('#open-box-edit-event').click();
            });
        });
    </script>

    {{--Delete event--}}
    <script type="text/javascript">
        function deleteEvent(live_id , event_key) {
            if(confirm('Are you sure delete it?')) {
                $.ajax({
                     url: '{{ URL::to('dashboard/live/delete-event') }}',
                     data: { live_id : live_id , event_key : event_key , _token : "{{csrf_token()}}"},
                     type: 'POST',
                     success: function(resp) {
                        if(resp.status) {
                            location.reload();
                        } else {
                            alert('Event not found or delete error!');
                        }
                     },
                     error: function(resp) {
                        alert('Something when wrong!');
                     }
                 });
            }
        }
    </script>

@stop