@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3 style="margin-left:3%;">{{ trans('content.create_live') }}
                <small>Create live / detailed information</small>
            </h3>
        </div>
        {!!Form::open(array('url' => URL::to('dashboard/live/create')
                                        , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
        <div class="col-md-12 create-live">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.event_home_team') }}</div>
                    <div class="panel-body">
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.event_home_team') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="homeTeamName" required>
                            </div>
                        </div>
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.logo_home') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="homeTeamLogo" required>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.event_away_team') }}</div>
                    <div class="panel-body">
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.event_away_team') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="awayTeamName" required>
                            </div>
                        </div>
                        <div class="row" style="padding-top:10px;">
                            <label class="col-sm-2 control-label">{{ trans('content.logo_away') }}</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" name="awayTeamLogo" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" id="postion_frame">{{ trans('content.live_information') }}</div>
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">{{ trans('content.live_league') }}</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="league" required>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">{{ trans('content.live_matchday') }}</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="matchday" required>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">{{ trans('content.live_live') }}</label>
                            <div class="switch-button sm showcase-switch-button col-sm-4">
                                <input class="live_live" id="switch-button-2" checked="" type="checkbox" name="have_report" value="true">
                                <label for="switch-button-2"></label>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">{{ trans('content.live_result') }}</label>
                            <div class="col-sm-4 box-result" status="off">
                                <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-home" value="0" readonly></div>
                                <div class="col-xs-2">-</div>
                                <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-away" value="0" readonly></div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">Time Start</label>
                            <div class="col-sm-4">
                                <div class="input-group date" id="datetimepicker1">
                                    <input name="date" type="text" class="form-control" data-date-format="YYYY-MM-DD H:m:s" required>
                                            <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control form-control-flat edit" name="description" type="text" cols="50" data-rows="100"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-sm-1" style="margin-right:5%">
                        <button type="submit" class="btn btn-purple">Save and back</button>
                    </div>
                    <div class="col-sm-2">
                        <input type="submit" name="live_now" value="Save and Live" class="btn btn-info">
                    </div>    
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

<script type="text/javascript">     // check live/no
    $('.create-live .live_live').change(function(){
        if($('.create-live .box-result').attr('status') == 'off') {
            $('.create-live .box-result').attr('status', 'on');
            $('.create-live .box-result').html('<div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-home" placeholder="home"></div> <div class="col-xs-2">-</div> <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-away" placeholder="away"></div>');
        } else {
            $('.create-live .box-result').attr('status', 'off');
            $('.create-live .box-result').html('<div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" name="result-home" value="0" readonly></div> <div class="col-xs-2">-</div> <div class="col-xs-5"><input class="form-control col-xs-1 result" type="number" value="0" name="result-away" readonly></div>');
        }
    });
</script>
@stop
