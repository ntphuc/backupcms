@extends('layouts.master')

@section('main_content')
<div class="warper container-fluid">
        <div class="page-header">
            <h3>
                FIX SMS
            </h3>
        </div>


        
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Fix Sms</div>
                    <div class="panel-body">
                        {!!Form::open(array('url' => route('fixing_sms')
                                        , 'class' => 'form-horizontal' , 'role' => 'form'))!!}
                        <input type="hidden"  name="id"  value="{{$content->id}}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Provider</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="provider_id"  required>
                                    @foreach($providers as $items )
                                        <option @if($items->id == $content->provider_id) selected @endif value="{{$items->id}}">{{$items->provider_name}}</option>
                                    @endforeach            
                                </select>
                            </div>
                        </div>
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Service Name</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" name="service_name"  required>
                                    <option value="">--Chưa chọn--</option>
                                    @foreach($services as $items )
                                        <option @if($items->service_name == $content->service_name) selected @endif value="{{$items->service_name}}">{{$items->service_name}}</option>
                                    @endforeach    
                                </select>
                            </div>
                        </div>
                        
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Short Code</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="short_code"  required><option value="{{$content->short_code}}">{{$content->short_code}}</option></select>
                            </div>
                        </div>
                        
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Time Start</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" data-date-format="YYYY-MM-DD H:m:00" id="started_at" name="started_at" value ="{{$content->started_at}}"  required>
                            </div>
                            <label class="col-sm-2 control-label">Time End</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" data-date-format="YYYY-MM-DD H:m:00" name="ended_at" value ="{{$content->ended_at}}" id="ended_at" required>
                            </div>
                        </div>
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Forward Now ? </label>
                            <div class="col-sm-8">
                                <div class="switch-button danger showcase-switch-button">
                                            <input id="switch-button-3"  type="checkbox" name="forward">
                                            <label for="switch-button-3"></label>
                                </div>
                            </div>
                        </div>        
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Content</label>
                            <div class="col-sm-8">
                                <textarea rows="10" class="form-control" name="content" maxlength="310" required>{{$content->content}}</textarea>
                            </div>
                        </div>
                        <hr class="dotted">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-purple">Submit</button>
                            </div>
                            <div class="col-sm-3">
                                <button type="reset" class="btn btn-info">Reset</button>
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>

        </div>

</div>
@stop    