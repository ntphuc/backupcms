@extends('layouts.master')

@section('main_content')
<div class="warper container-fluid">
        <div class="page-header">
            <h3>
                CHOOSE SERVICE FOR SMS
            </h3>
        </div>


        
        <div class="row">
            
            	<div role="alert" class="alert alert-success fade in">
                              <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                              <h4>Choose Service ! You must have choose 1 service!</h4>
                              <p>
                                You can choose 1 service in three button bellow . <br>Click and redirect to SMS Platform .<br> Have a nice day ! 
                              <p>
                                <a href="{{route('sms').'?short_code=700'}}"><button class="btn btn-success" type="button">TAKE SERVICE 700-KQTT-TRAITHIVANG</button></a>
                                <a href="{{route('sms').'?short_code=565'}}"><button class="btn btn-success" type="button">TAKE SERVICE 565-TUYENGAP-TRAITHIVANG</button></a>
                                <a href="{{route('sms').'?short_code=15602'}}"><button class="btn btn-success" type="button">TAKE SERVICE 15602-HALOSOKA-TRAITHIVANG</button></a>
                              </p>
                </div>
        </div>    

</div>
@stop                    