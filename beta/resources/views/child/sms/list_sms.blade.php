@extends('layouts.master')
@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header"><h3>SMS Information {{$service_name}}</h3></div>
        <div class="row">
            <div class="col-md-2" style="margin-bottom:10px;">
            <a href="{{route('send_sms').'?short_code='.$short_code}}">
                <button class="btn btn-success btn-block " type="button">
                    SEND SMS
                </button>
            </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" style="margin-bottom:10px;">
                <a href="{{route('sms')}}">
                    <button class="btn btn-success btn-block " type="button">
                        CHANGE SHORTCODE
                    </button>
                </a>    
            </div>
        </div>    
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Controls Table</div>
                    <div class="panel-body">
                        {!!Form::open(array('url' => route('delete_sms')
                                        , 'class' => 'form-inline' , 'role' => 'form ' , 'method' => 'POST'))!!}
                        <input type="hidden" value="{{Input::get('short_code')}}" name="short_code">                    
                        <div class="row" style="margin-bottom: 10px;    ">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select class="form-control  form-control-flat input-sm" name="select_action">
                                        <option value="Delete">Delete</option>
                                    </select>
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Apply</button>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="1">
                                    <label class="cr-styled">
                                        <input type="checkbox" id="check_all">
                                        <i class="fa"></i>
                                    </label>
                                </th>
                                <th width="1">Service</th>
                                <th width="1">Content</th>
                                <th width="1">Short Code</th>
                                <th width="1">Time Start</th>
                                <th width="1">Time End</th>
                                <th width="1">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_sms as $sms)
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" ng-model="todo.done" name="listId[]"
                                                   value="{!! $sms->id !!}">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>{{$sms->service_name}}</td>
                                    <td style="text-align: center">{{$sms->content}}</td>
                                    <td>{{$short_code}}</td>    
                                    <td>{{date('d/m/Y H:i:s' ,strtotime($sms->started_at))}}</td>
                                    <td>{{date('d/m/Y H:i:s' ,strtotime($sms->ended_at))}}</td>
                                    <td><a href="{{route('fix_sms').'/'.$sms->id.'?short_code='.$sms->short_code}}"><i
                                                    class="glyphicon glyphicon-edit"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                        {!! $all_sms->appends(['short_code' => Input::get('short_code')])->render() !!}

                    </div>
                </div>
            </div>
            @include('popup.popup_addmedia')
            @include('popup.popup_playmedia')
        </div>
        </div>
@stop

