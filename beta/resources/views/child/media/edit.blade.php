@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3 style="margin-left:1.5%;">Edit Media
                <small>edit media / detailed information</small>
            </h3>
        </div>
        {!!Form::open(array('url' => route('media.postUpdate')
                                        , 'class' => 'form-horizontal' , 'role' => 'form'))!!}
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" id="postion_frame">{{trans('content.upload_media')}}</div>
                <div class="panel-body">
                    <div class="row" style="padding-top:10px;">
                        <label class="col-sm-2 control-label">{{trans('content.name_media')}}</label>
                        <div class="col-xs-10">
                            <input class="form-control" value="{{$media_data->media_name}}" type="text" name="media_name" required>
                            <input type="hidden" class="form-control" id="txtid" placeholder="Id" value="{{$media_data->id}}" name="id">
                        </div>
                    </div>
                    <hr class="dotted">
                    @include('partials.button-upload' ,  ['video_option' =>true, 'image_option' => false , 'data' => $media_data])
                    <hr class="dotted">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading" id="postion_frame">{{trans('content.upload_media')}}</div>
                <div class="panel-body">
                    @include('partials.button-upload' ,  ['video_option' =>false, 'image_option' => true, 'data' => $media_data ])
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="padding: 0px;">
                <div class="form-group" style="margin-left: 0px;padding: 0px">
                    <div class="col-sm-12" style="margin-left: 0px;padding: 0px">
                        <button type="submit" class="btn btn-purple" style="margin-left: 0px;">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
    @include('partials.upload')
@stop

