@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3>Upload Media
                <small>upload media / detailed information</small>
            </h3>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" id="postion_frame">Upload File</div>
            <div class="panel-body">
                {!!Form::open(array('url' => URL::to('dashboard/media/process-media')
                                , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                <div class="row" style="padding-top:10px;">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-xs-2">
                        <button id="uploadBtn-Video" class="btn btn-large btn-primary">Choose File</button>
                    </div>


                    <input type="hidden" value="{{uniqid()}}" name="media_name" required>
                    <input type="hidden" value="" name="media_path" id="video-src" required>
                    <input type="hidden" value="" name="media_thumb" id="thumb-src" required>
                    <input type="hidden" value="-1" name="media_status" >
                    <input type="hidden" value="" name="media_mime_type" id="mime" >
                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">


                    <div class="col-xs-12">
                        <div id="progressOuter-Video" class="progress progress-striped active" style="display:none;">
                            <div id="progressBar-Video" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            </div>
                        </div>
                    </div>
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-xs-7">
                        <video id="respone-video"  style="display: none"></video>
                    </div>
                </div>
                <div class="row" style="padding-top:10px;">
                    <div class="col-xs-12">
                        <div id="msgBox-Video">
                        </div>
                    </div>
                </div>
                <hr class="dotted">
                <div class="row" style="padding-top:10px;display: none;">
                    <label class="col-sm-2 control-label">Đăng Ảnh</label>
                    <div class="col-xs-2">
                        <button id="uploadBtn-Thumb" class="btn btn-large btn-primary">Choose File</button>
                    </div>
                    <div class="col-xs-12">
                        <div id="progressOuter-Thumb" class="progress progress-striped active" style="display:none;">
                            <div id="progressBar-Thumb" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            </div>
                        </div>
                    </div>
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-xs-7">
                        <img id="respone-img"  style="display: none ; width:100%;">
                    </div>
                </div>
                <div class="row" style="padding-top:10px;">
                    <div class="col-xs-12">
                        <div id="msgBox-Thumb">
                        </div>
                    </div>
                </div>

                <div class="form-group" style="display: none">
                    <label class="col-sm-2 control-label"></label>

                    <div class="col-sm-1">
                        <button type="submit" id="submit-form" class="btn btn-purple">Submit</button>
                    </div>
                    <div class="col-sm-3">
                        <button type="reset" class="btn btn-info">Reset</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('partials.media_upload_lib')
@stop
