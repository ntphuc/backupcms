@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header"><h3>Media Information</h3></div>
        <div class="row">
            <div class="col-md-2" style="margin-bottom:10px;">
                <button class="btn btn-success btn-block " type="button" data-toggle="modal" data-target="#addNote">
                    Quick New Media
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Controls Table</div>
                    <div class="panel-body">
                    @include('partials.notify')
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <select class="form-control input-sm  form-control-flat" id="per_page">
                                        @if(!Session::has('media_per_page'))
                                            <option>Default Per Page</option>
                                        @endif
                                        <option @if(Session::has('media_per_page') && Session::get('media_per_page') == '20') selected @endif value="05">05</option>
                                        <option @if(Session::has('media_per_page') && Session::get('media_per_page') == '20') selected @endif value="10">10</option>
                                        <option @if(Session::has('media_per_page') && Session::get('media_per_page') == '20') selected @endif value="20">20</option>
                                        <option @if(Session::has('media_per_page') && Session::get('media_per_page') == '30') selected @endif value="30">30</option>
                                        <option @if(Session::has('media_per_page') && Session::get('media_per_page') == '40') selected @endif value="40">40</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <select class="form-control input-sm  form-control-flat" id="media_by_user" name="user">
                                    <?php
                                    $user = App\Models\User::all();
                                    ?>
                                    <option @if(!Session::has('media_by_user')) selected="selected" @endif value="all">All</option>
                                    @foreach($user as $value)
                                        <option value="{{$value->id}}"
                                                @if(Session::get('media_by_user') == $value->id) selected @endif>{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3 col-lg-offset-5">
                                {!!Form::open(array('url' => route('media')
                                        , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm  form-control-flat"
                                           placeholder="Search record here..." name="q">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm btn-flat" type="submit">Go!</button>
                                      </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!!Form::open(array('url' => route('media.anyAction')
                                        , 'class' => 'form-inline' , 'role' => 'form ' , 'method' => 'POST'))!!}
                        <div class="row" style="margin-bottom: 10px;    ">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select class="form-control  form-control-flat input-sm" name="select_action">
                                        <option value="Actions">Actions</option>
                                        <option value="Delete">Delete</option>
                                        <option value="Active">Active</option>
                                        <option value="Deactive">Deactive</option>
                                    </select>
                                    <button class="btn btn-default btn-sm btn-flat" type="submit">Apply</button>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="1">
                                    <label class="cr-styled">
                                        <input type="checkbox" id="check_all">
                                        <i class="fa"></i>
                                    </label>
                                </th>
                                <th width="1">Avatar</th>
                                <th width="1">Name</th>
                                <th width="1">Created By</th>
                                <th width="1">Created At</th>
                                <th width="1">Status</th>
                                <th width="1">Review</th>
                                <th width="1">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($media_datas as $media)
                                <?php
                                $user_create = App\Models\User::find($media->user_id);
                                ?>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" ng-model="todo.done" name="listId[]"
                                                   value="{!! $media->_id !!}">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>
                                        <img src="{{$media->media_thumbnail}}" style="width:60px;height: 40px;">
                                    </td>
                                    <td style="text-align: center">{{$media->media_name}}</td>
                                    @if(isset($user_create))
                                        <td width="10">{{$user_create->name}}</td>
                                    @else
                                        <td width="10">None</td>
                                    @endif
                                    <td>{{date('D d/m/Y' ,strtotime($media->created_at))}}</td>
                                    <td>
                                        @if($media->media_status == 1)
                                            <i class="fa fa-check" style="color:#428BCA "></i>
                                        @elseif($media->media_status == 0)
                                            <i class="fa fa-close" style="color:#2b2f3e "></i>
                                        @endif
                                    </td>
                                    <td width="1"><i data-toggle="modal" style="cursor: pointer"
                                                     class="glyphicon glyphicon-play-circle" data-video=""
                                                     onclick="Review('{{env('link_static') . str_replace(env('folder_remove') , '' , $media->media_path )}}')"></i>
                                    </td>
                                    <td><a href="{{route('media.getUpdate').'/'.$media->_id}}"><i
                                                    class="glyphicon glyphicon-edit"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                        <div class="row">
                            <div class="col-lg-3">
                                {!!Form::open(array('url' => route('media')
                                        , 'class' => '' , 'role' => 'form ' , 'method' => 'GET'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm  form-control-flat"
                                           placeholder="Search record here..." name="q">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm btn-flat" type="submit">Go!</button>
                                      </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!! $media_datas->appends(['q' => Input::get('q')])->render() !!}

                    </div>
                </div>
            </div>
            @include('popup.popup_addmedia')
            @include('popup.popup_playmedia')
        </div>
        </div>
        <script>
            $('#per_page').on('change', function () {
                $value = $(this).val();
                $.ajax({
                    method: "POST",
                    url: "{{route('media.anyAction')}}",
                    data: {session: $value, _token: "{{csrf_token()}}"},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
            $('#media_by_user').on('change', function () {
                $value = $(this).val();
                $.ajax({
                    method: "POST",
                    url: "{{route('media.anyAction')}}",
                    data: {user: $value, _token: "{{csrf_token()}}"},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
            $('.close_reload').on('click', function () {
                location.reload();
            });
            function Review($file_path) {
                $('#video_review').attr('src', $file_path);
                $('#button_review').trigger('click');
            }
        </script>
        <script type="text/javascript">
            $("#check_all").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        </script>
@stop

