@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2" style="margin-bottom:10px;">
                        <button class="btn btn-success btn-block " type="button" data-toggle="modal" data-target="#addNote">
                            Quick New Media
                        </button>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Controls Table</div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                            @foreach($media_datas as $media)
                                <?php
                                $user_create = App\Models\User::find($media->user_id);
                                ?>
                                <tr>

                                    <td>
                                        <img src="{{$media->media_thumbnail}}" style="width:60px;height: 40px;">
                                    </td>
                                    <td style="text-align: left">{{$media->media_name}}</td>
                                    <td width="1"><i data-toggle="modal" style="cursor: pointer"
                                                     class="glyphicon glyphicon-play-circle" data-video=""
                                                     onclick="Review('{{env('link_static') . str_replace(env('folder_remove') , '' , $media->media_path )}}')"></i>
                                    </td>
                                    <td><i class="fa fa-plus"
                                           data-url="{{str_replace(env('folder_remove') , '' , env('link_static').$media->media_path)}}"
                                           cloud_id="" style="cursor: pointer"></i></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! $media_datas->appends(['popup' => 'true'])->render() !!}

                    </div>
                </div>
            </div>
            @include('popup.popup_addmedia')
            @include('popup.popup_playmedia')
            <script>
                $('.fa-plus').on('click', function () {
                    if ($(this).attr('cloud_id') == '') {

                        $video = '<p><br></p><p><iframe controls src="' + $(this).attr('data-url') + '"   frameborder="0" allowfullscreen="" width="640" height="360"></iframe></p>';
                        if($(".fr-view",parent.document).append($video)){
                            $(".close-upload-media",parent.document).trigger('click') ;
                        }

                    }
                });
            </script>
        </div>
        <script>
            $('.close_reload').on('click', function () {
                location.reload();
            });
            function Review($file_path) {
                $('#video_review').attr('src', $file_path);
                $('#button_review').trigger('click');

            }
        </script>

@stop
