@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3>Upload Media
                <small>upload media / detailed information</small>
            </h3>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" id="postion_frame">{{trans('content.upload_media')}}</div>
            <div class="panel-body">
                {!!Form::open(array('url' => URL::to('dashboard/media/create')
                                , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                @include('partials.button-upload' ,  ['video_option' => true , 'image_option' => true ])
                <hr class="dotted">
                <div class="row" style="padding-top:10px;">
                    <label class="col-sm-2 control-label">{{trans('content.name_media')}}</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" name="media_name">
                    </div>
                </div>
                <hr class="dotted">
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-purple">Submit</button>
                    </div>
                    <div class="col-sm-3">
                        <button type="reset" class="btn btn-info">Reset</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('partials.upload')
@stop

