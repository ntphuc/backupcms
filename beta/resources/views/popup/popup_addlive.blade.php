    <div class="modal fade" id="addLive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_reload" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.create_live')}}</h4>
            </div>
            <div class="modal-body">

                {!!Form::open(array('url' => URL::to('dashboard/live/create')
                                        , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.event_home_team') }}</div>
                            <div class="panel-body">
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.event_home_team') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control" type="text" name="homeTeamName" required>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.logo_home') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control" type="text" name="homeTeamLogo" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.event_away_team') }}</div>
                            <div class="panel-body">
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.event_away_team') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control" type="text" name="awayTeamName" required>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.logo_away') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control" type="text" name="awayTeamLogo" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.live_description') }}</div>
                            <div class="panel-body">
                        <textarea class="form-control form-control-flat edit" name="description" type="text" cols="50"
                                  data-rows="100"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.live_time_match') }}</div>
                            <div class="panel-body">
                                <div class="input-group date" id="datetimepicker">
                                    <input type="text" class="form-control">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date &amp; Time Picker</label>
                        <div class="col-sm-3">
                            <div class="input-group date" id="datetimepicker">
                                <input type="text" class="form-control">
                                            <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group home-team">
                        <label>{{ trans('content.live_live') }}</label>
                        <div class="switch-button sm showcase-switch-button">
                            <input id="switch-button-1" checked="" type="checkbox" name="have_report" value="true">
                            <label for="switch-button-1"></label>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-purple">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>