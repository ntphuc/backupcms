<a class="glyphicon glyphicon-edit open-popup-edit" data-toggle="modal" data-target="#editLive" style="display: none;"></a>
<div class="modal fade" id="editLive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_reload" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.edit_live')}}</h4>
            </div>
            <div class="modal-body">

                {!!Form::open(array('url' => URL::to('dashboard/live/edit')
                                        , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                <div class="col-xs-12">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.event_home_team') }}</div>
                            <div class="panel-body">
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.event_home_team') }}</label>
                                    <div class="col-xs-10">
                                        <input class="live_id" type="hidden" name="live_id" value="">
                                        <input class="form-control homeTeamName" type="text" name="homeTeamName" value="" required>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.logo_home') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control homeTeamLogo" type="text" name="homeTeamLogo" value="" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.event_away_team') }}</div>
                            <div class="panel-body">
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.event_away_team') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control awayTeamName" type="text" name="awayTeamName" value="" required>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:10px;">
                                    <label class="col-sm-2 control-label">{{ trans('content.logo_away') }}</label>
                                    <div class="col-xs-10">
                                        <input class="form-control awayTeamLogo" type="text" name="awayTeamLogo" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="postion_frame">{{ trans('content.live_information') }}</div>
                            <div class="panel-body">
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">{{ trans('content.live_league') }}</label>
                                    <div class="col-sm-4">
                                        <input class="form-control league" type="text" name="league" value="" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">{{ trans('content.live_matchday') }}</label>
                                    <div class="col-sm-4">
                                        <input class="form-control matchday" type="text" name="matchday" value="" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">{{ trans('content.live_live') }}</label>
                                    <div class="col-sm-4">
                                        <input class="have_report" type="checkbox" name="have_report" value="true">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">{{ trans('content.live_result') }}</label>
                                    <div class="col-sm-4 box-result" status="off">
                                        <div class="col-xs-5"><input class="form-control col-xs-1 result result-home" type="number" name="result-home" value="0" readonly></div>
                                        <div class="col-xs-2">-</div>
                                        <div class="col-xs-5"><input class="form-control col-xs-1 result result-away" type="number" name="result-away" value="0" readonly></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">Time Start</label>
                                    <div class="col-sm-4">
                                        <div class="input-group date" id="datetimepicker1">
                                            <input name="date" type="text" class="form-control date" data-date-format="YYYY-MM-DD H:m:s" value="" required>
                                            <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control form-control-flat edit" name="description" type="text" cols="50" data-rows="100"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-purple">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

