<button id="button_review" type="button" data-toggle="modal" data-target="#review" style="display: none;"></button>

<div class="modal fade" id="review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.review_media')}}</h4>
            </div>
            <div class="modal-body">

                <video controls="controls" src="" style="width : 100%; height: 300px;" id="video_review"></video>

            </div>
        </div>
    </div>
</div>