<button id="button_xuatban_post" type="button" data-toggle="modal" data-target="#review-post" style="display: none;"></button>
<div class="modal fade" id="review-post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.zone_post')}}</h4>
            </div>
            <div class="modal-body">

                <iframe id="post_content" src=""
                        style="width: 100%;height: 600px;"></iframe>

            </div>
        </div>
    </div>
</div>