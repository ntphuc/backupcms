<div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog popup-edit-event">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Event</h4>
            </div>
            <div class="modal-body">

                {!!Form::open(array('url' => URL::to('dashboard/live/edit-event')
                                            , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                <div class="col-sm-12 create-live">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ trans('content.edit_event') }}</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Type</label>
                                <select name="type" class="form-control chosen-select" data-placeholder="Choose a Country">
                                    <option value="text" >Text</option>
                                    <option value="goal" >Goal</option>
                                    <option value="og_goal" >og_goal</option>
                                    <option value="pen" >pen</option>
                                    <option value="miss_pen" >miss_pen</option>
                                    <option value="red_card" >Red Card</option>
                                    <option value="yellow_card" >Yellow Card</option>
                                </select>

                            </div>
                            <hr class="dotted">
                            <div class="form-group home-team">
                                <label>{{ trans('content.event_home_team') }}</label>
                                <div class="switch-button showcase-switch-button">
                                    <input id="switch-button-6" name="event_of_team" type="radio" value="home" >
                                    <label for="switch-button-6"></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('content.event_away_team') }}</label>
                                <div class="switch-button showcase-switch-button">
                                    <input id="switch-button-7" name="event_of_team" type="radio" value="away" >
                                    <label for="switch-button-7"></label>
                                </div>
                            </div>
                            <hr class="dotted">
                            <div class="form-group">
                                <label>{{ trans('content.event_player') }}</label>
                                <input type="text" name="player" class="form-control player" value="" placeholder="Ronando">
                                <input type="hidden" name="live_id" class="form-control" value="">
                                <input type="hidden" name="event_key" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>{{ trans('content.event_time') }}</label>
                                <input type="number" name="minutes" class="form-control" value="" placeholder="00:00" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ trans('content.event_content_event') }}</div>
                        <div class="panel-body">
                     <textarea class="form-control form-control-flat " name="content" id="edit" type="text" cols="50"
                               data-rows="100">@if(isset($event['content'])) {{$event['content']}} @endif</textarea>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12">
                    <button class="btn btn-success btn-block" type="submit">Edit</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>