<button class="btn btn-success btn-block" type="button" id="insert_video_button" data-toggle="modal"
        data-target="#insertVideo" style="display: none">Add New Event
</button>
<div class="modal fade" id="insertVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_reload close-upload-media" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.upload_media')}}</h4>
            </div>
            <div class="modal-body">
                <iframe src="{{route('media').'?popup=true'}}" style="width: 100%;height: 500px;" ></iframe>
            </div>
        </div>
    </div>
</div>