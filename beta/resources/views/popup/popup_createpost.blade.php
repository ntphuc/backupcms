<div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_reload" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.zone_post')}}</h4>
            </div>
            <div class="modal-body">

                <iframe src="{{URL::to('dashboard/post/create-post')}}"
                        style="width: 100%;height: 600px;"></iframe>

            </div>
        </div>
    </div>
</div>