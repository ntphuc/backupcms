<button class="button_review_live" type="button" data-toggle="modal" data-target="#button_review_live" style="display: none;"></button>
<div class="modal fade" id="button_review_live" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.live_information')}}
                    <div class="col-sm-2 publish"><button type="button" class="btn btn-success btn-block" status="off">Publish</button></div>
                </h4>

            </div>
            <div class="modal-body">
                <div class="col-sm-12 box-reviewlive">
                    <div class="col-xs-12 logo">
                        <div class="col-md-6 col-md-push-3 box" style="margin-left:4%;">
                            <div class="col-xs-5 logo-left">
                                <img style="width : 100%;" src="http://125.212.193.90:2016//23-02-2016-16-02-11-home-team-logo.png">
                            </div>
                            <div class="col-xs-2 vs">
                                <img style="width : 30px;margin-top:40px;" src="http://125.212.193.90:2016///img/56c2f7a9e628e.png">
                            </div>
                            <div class="col-xs-5 logo-right">
                                <img style="width : 100%;" src="http://125.212.193.90:2016//23-02-2016-16-02-12-away-team-logo.png">
                            </div>
                        </div>
                    </div>
                    <style>
                        .col-xs-4 {
                            background : #ffffff ; 
                        }
                    </style>
                    <div class="col-xs-12 result">
                    <div class="col-md-12">
                            <span class="ti-so" style="text-align:center;overflow:hidden;float: left;margin-left: 51%;"><h3 class="text-purple"> 0 - 0 </h3></span>
                        </div>
                    </div>
                    <div class="col-xs-12 table-review-live">
                        <table class="table table-bordered">
                            <tbody>
                                    <tr class="active">
                                            <input type="hidden" name="event_key" class="event_key" value="28">
                                            <td  style="background-color : transparent;" class="col-xs-1"><label>28'</label></td>
                                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"><label>Hoàng Vũ</label></td>
                                            <td  style="background-color : transparent;    border-left: none;" class="col-xs-1 icon-event"><i class="icon-goal"></i></td>
                                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"></td>
                                    </tr>
                                    <tr class="active">
                                         <input type="hidden" name="event_key" class="event_key" value="20">
                                            <td  style="background-color : transparent;" class="col-xs-1"><label>20'</label></td>
                                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"></td>
                                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-pen"></i></td>
                                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"><label>Đức đầu to</label></td>
                                    </tr>
                                    <tr class="active">
                                            <input type="hidden" name="event_key" class="event_key" value="28">
                                            <td  style="background-color : transparent;" class="col-xs-1"><label>28'</label></td>
                                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"><label>Hoàng Vũ</label></td>
                                            <td  style="background-color : transparent;    border-left: none;" class="col-xs-1 icon-event"><i class="icon-goal"></i></td>
                                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"></td>
                                    </tr>
                                    <tr class="active">
                                         <input type="hidden" name="event_key" class="event_key" value="20">
                                            <td  style="background-color : transparent;" class="col-xs-1"><label>20'</label></td>
                                            <td style="background-color : transparent;border-right: none;" class="col-xs-4"></td>
                                            <td  style="background-color : transparent;border-left: none;" class="col-xs-1 icon-event"><i class="icon-none"></i></td>
                                            <td  style="background-color : transparent;border-right: none;" class="col-xs-1 icon-event"><i class="icon-pen"></i></td>
                                            <td style="background-color : transparent;border-left: none;" class="col-xs-4"><label>Đức đầu to</label></td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                {!!Form::open(array('url' => route('ReleasePushToQueue')
                                        , 'class' => 'form-horizontal' , 'role' => 'form '))!!}    
                <div class="col-xs-12 published">
                    <div class="panel panel-default">
                        <div class="panel-heading" id="postion_frame">Published</div>
                        <div class="panel-body">
                            <input class="live_id_publish" name="live_id" type="hidden" value="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="1">
                                        <label class="cr-styled">
                                            #
                                        </label>
                                    </th>
                                    <th width="1">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" name="listWeb[]" value="http://bongdaquocte.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>bdqt.vn</td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input class="inter" type="checkbox" name="listWeb[]" value="http://inter.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>inter.vn</td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input class="vieclamnambo" type="checkbox" name="listWeb[]" value="http://vieclamnambo.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>vieclamnambo.vn</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3" style="margin-bottom:10px;">
                            <div class="col-xs-12">
                                <button class="btn btn-success btn-block" type="submit">Publish now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}    
        </div>
    </div>
</div>

<script type="text/javascript">     // show/hide box publish live
    $('#button_review_live .publish button').click(function(){      // click vao publish thi hien bang publish len
        if($(this).attr('status') == 'off') {
            $('#button_review_live .box-reviewlive').slideUp(300);
            $('#button_review_live .modal-dialog form').slideDown(300);
            $(this).html('Back');
            $(this).attr('status', 'on');
        } else {
            $('#button_review_live .box-reviewlive').slideDown(300);
            $('#button_review_live .modal-dialog form').slideUp(300);
            $(this).html('Publish');
            $(this).attr('status', 'off');
        }
    });

    $('#button_review_live .close').click(function(){       // click vao nut dong cua so thi dong bang publish
        $('#button_review_live .box-reviewlive').slideDown(300);
        $('#button_review_live .modal-dialog form').slideUp(300);
        $('#button_review_live .publish button').html('Publish');
        $('#button_review_live .publish button').attr('status', 'off');
    });
    $("html").click(function (e) {                          // click ra ngoai dong cua so thi dong bang publish
        if ($('#button_review_live').is(":visible")){

        }else {
            $('#button_review_live .box-reviewlive').slideDown(300);
            $('#button_review_live .modal-dialog form').slideUp(300);
            $('#button_review_live .publish button').html('Publish');
            $('#button_review_live .publish button').attr('status', 'off');
        }
    });
</script>