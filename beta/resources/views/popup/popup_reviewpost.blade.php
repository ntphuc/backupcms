<button id="button_review_post" type="button" data-toggle="modal" data-target="#review-post" style="display: none;"></button>
<div class="modal fade" id="review-post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('content.zone_post')}}
                    <div style="float: right" class="col-sm-2 publish"><button type="button" class="btn btn-success btn-block" status="off">Publish</button></div>
                </h4>
            </div>

            <div class="modal-body review-post">
                <div class="col-xs-12 box-review-post">

                </div>

                {!!Form::open(array('url' => route('ReleasePushToQueue')
                                                    , 'class' => 'form-horizontal' , 'role' => 'form '))!!}
                    <div class="col-xs-12 published">
                    <div class="panel panel-default">
                        <div class="panel-heading" id="postion_frame">Published</div>
                        <div class="panel-body">
                            <input class="post_id_publish" name="post_id" type="hidden" value="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="1">
                                        <label class="cr-styled">
                                            #
                                        </label>
                                    </th>
                                    <th width="1">Name</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" name="listWeb[]" value="http://bongdaquocte.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>bdqt.vn</td>

                                </tr>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" name="listWeb[]" value="http://inter.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>inter.vn</td>

                                </tr>
                                <tr>
                                    <td>
                                        <label class="cr-styled">
                                            <input type="checkbox" name="listWeb[]" value="http://vieclamnambo.vn" ng-model="todo.done">
                                            <i class="fa"></i>
                                        </label>
                                    </td>
                                    <td>vieclamnambo.vn</td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-bottom:10px;">
                            <div class="col-xs-12">
                                <button style="width: 130px" class="btn btn-success btn-block" type="submit">Publish now</button>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                {!! Form::close() !!}
        </div>
    </div>
</div>

<script type="text/javascript">     // show/hide box publish live
    $('#review-post .publish button').click(function(){      // click vao publish thi hien bang publish len
        if($(this).attr('status') == 'off') {
            $('#review-post .box-review-post').slideUp(300);
            $('#review-post .modal-body.review-post > form').slideDown(300);
            $(this).html('Back');
            $(this).attr('status', 'on');
        } else {
            $('#review-post .box-review-post').slideDown(300);
            $('#review-post .modal-body.review-post > form').slideUp(300);
            $(this).html('Publish');
            $(this).attr('status', 'off');
        }
    });

    $('#button_review_live .close').click(function(){       // click vao nut dong cua so thi dong bang publish
        $('#review-post .box-review-post').slideDown(300);
        $('#review-post .modal-body.review-post > form').slideUp(300);
        $('#review-post .publish button').html('Publish');
        $('#review-post .publish button').attr('status', 'off');
    });
    $("html").click(function (e) {                          // click ra ngoai dong cua so thi dong bang publish
        if ($('#review-post').is(":visible")){

        }else {
            $('#review-post .box-review-post').slideDown(300);
            $('#review-post .modal-body.review-post > form').slideUp(300);
            $('#review-post .publish button').html('Publish');
            $('#review-post .publish button').attr('status', 'off');
        }
    });
</script>