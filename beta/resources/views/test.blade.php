@extends('layouts.master')

@section('main_content')
    <div class="warper container-fluid">
        <div class="page-header">
            <h3>Extra Info
                <small>show extra / detailed information</small>
            </h3>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Report Data</div>
            <div class="panel-body">
                <label for="feature_image">Feature Image</label>
                <input type="text" id="feature_image" name="feature_image" value=""/>
                <a href="" class="popup_selector" data-inputid="1">Select Image</a>
            </div>
        </div>
    </div>
@stop

@section('custom_script')
    <script type="text/javascript">

    </script>
@stop