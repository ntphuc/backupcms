<header class="top-head container-fluid">
    <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <form role="search" class="navbar-left app-search pull-left hidden-xs">
        <input type="text" placeholder="Enter email..." class="form-control form-control-circle">
    </form>
    <ul class="nav-toolbar">
        <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="badge">3</span></a>

            <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
                <div class="panel-heading">
                    Notification
                </div>
                <div class="list-group">

                </div>
            </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>

            <div class="dropdown-menu lg pull-right arrow panel panel-default arrow-top-right">
                <div class="panel-heading">
                    Control Panel
                </div>
                <div class="panel-body text-center">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4"><a href="#" class="text-yellow"><span class="h2"><i
                                            class="fa fa-bar-chart"></i></span>

                                <p class="text-gray">Report</p></a></div>
                        <div class="col-xs-12 visible-xs-block">
                            <hr>
                        </div>
                        {{--<div class="col-xs-6 col-sm-4"><a href="{!! route('user.getProfile') !!}"--}}
                                                          {{--class="text-primary"><span class="h2"><i--}}
                                            {{--class="fa fa-user"></i></span>--}}

                                {{--<p class="text-gray">Profile</p></a></div>--}}
                        <div class="col-xs-6 col-sm-4"><a href="{!! url('signout') !!}" class="text-info"><span
                                        class="h2"><i
                                            class="fa fa-sign-out"></i></span>

                                <p class="text-gray">Sign Out</p></a></div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</header>
<!-- Header Ends -->