<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trai Thi Vang Jsc - Content Management System</title>

    <meta name="description" content="">
    <meta name="author" content="TanlinhND">

    <!-- Bootstrap core CSS -->
    {!! HTML::style('assets/css/bootstrap/bootstrap.css') !!}

            <!-- Typeahead Styling  -->
    {!! HTML::style('assets/css/plugins/typeahead/typeahead.css') !!}

            <!-- TagsInput Styling  -->
    {!! HTML::style('assets/css/colorbox.css') !!}
    {!! HTML::style('assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}

            <!-- Chosen Select  -->
    {!! HTML::style('assets/css/plugins/bootstrap-chosen/chosen.css') !!}

            <!-- DateTime Picker  -->
    {!! HTML::style('assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css') !!}

    {!! HTML::style('assets/css/plugins/calendar/calendar.css') !!}
    {!! HTML::style('assets/css/app/timeline.css') !!}

            <!-- Fonts  -->
    {!! HTML::style('http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300') !!}

            <!-- Base Styling  -->
    {!! HTML::style('assets/css/app/app.css') !!}

    {!! HTML::style('assets/css/app/custom.css') !!}
    {!! HTML::style('assets/css/switch-buttons/switch-buttons.css') !!}

    {!! HTML::style('assets/css/plugins/datatables/jquery.dataTables.css') !!}
    {!! HTML::script('assets/js/jquery/jquery-1.9.1.min.js') !!}
    {!! HTML::style('assets/css/fraola_editor.min.css') !!}
    {!! HTML::style('assets/css/fraola_style.min.css') !!}

            <!--holder-js-->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
          class="holderjs">
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css' class="holderjs">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/code_view.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/colors.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/emoticons.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/image_manager.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/image.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/line_breaker.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/table.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/char_counter.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/video.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/fullscreen.css">
    <link rel="stylesheet" href="{{URL::to('/assets/css/')}}/plugins/file.css">
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.core.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.tags.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.autocomplete.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.focus.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.prompt.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.arrow.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::to('assets/css/textext/textext.plugin.arrow.css') }}" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {!! HTML::style('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') !!}
    {!! HTML::style('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') !!}
    <![endif]-->
<style>
    table thead tr th {
        text-align: center;
        vertical-align: middle;
    }

    table tbody tr td {
        text-align: center;
        vertical-align: middle;
    }

    .input_td {
        width: 50px;
        margin: 0 auto;
        text-align: center;
        vertical-align: middle;
    }

    /*Modul Post */
    .new-posts .new-posts-left > div,
    .new-posts .new-posts-right > div {
        padding: 0;
    }
    .new-posts .text-core .text-wrap {
        width: 100% !important;
    }

    .new-posts .text-core .text-wrap textarea {
        width: 100% !important;
        min-height: 34px;
    }

    .new-posts .select_option {
        min-height: 42px;
        max-height: 200px;
        overflow: auto;
        padding: 0 .9em;
        background-color: #fdfdfd;
    }

    .new-posts .select_option ul {
        padding: 0;
    }

    .new-posts .select_option ul li {
        list-style: none;
        margin: 0;
        padding: 0;
        line-height: 22px;
        word-wrap: break-word;
    }

    .new-posts .text-core .text-wrap .text-tags .text-tag {
        float: left;
        margin-left: 3px;
        margin-top: 2px;
    }

    .new-posts .text-core .text-wrap .text-tags span {
        font-size: 14px;
        line-height: 21px;
    }

    .new-posts .text-core .text-wrap .text-tags .text-tag .text-button a.text-remove {
        right: 1px;
        top: 6px;
        width: 13px;
        height: 10px;
    }

    .text-core .text-wrap .text-tags .text-tag .text-button {
        height: 24px !important;
    }

    .new-posts .category .panel-body {
        padding: 0;
    }

    .new-posts .category .panel-body table tr td label{
        height: 21px;
        overflow: hidden;
        cursor: pointer;
        text-align: -webkit-auto;
        display: -webkit-box !important;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
    }
    .new-posts .text-core .text-wrap .text-tags .text-tag {
        float: left;
        margin-left: 3px;
        margin-top: 2px;
        overflow: hidden;
    }

    .box-xuat-ban {
        display: none;
    }

    .modal.in .modal-dialog {
        -webkit-transform: translate3d(0, 0, 0);
        -o-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        width: 70%;
    }
    #review-post .modal-body.review-post {
        display: inline-block;
        width: 100%;
    }
  


    /*@media(max-width: 500px) {*/
        /*.new-posts .froala-element.f-basic {*/
            /*height: 250px !important;*/
        /*}*/
    /*}*/
    /*ENd Modul Post*/
</style>
</head>
<body>