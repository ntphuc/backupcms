{{--Lib auto complete tags - Hoang Hung--}}
<script src="{{ URL::to('assets/js/textext/textext.core.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.tags.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.autocomplete.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.suggestions.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.filter.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.focus.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.prompt.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.ajax.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::to('assets/js/textext/textext.plugin.arrow.js') }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var list = [];
    $(document).ready(function(){
        $('#textarea').keyup(function(){
            $key = $('#textarea').val();
            $.ajax({
                url: '{{ URL::to("dashboard/post/tag") }}',
                type: 'POST',
                data : { data_post : $key  , _token : "{{csrf_token()}}" }  ,
                success: function(resp) {
                 resp = jQuery.parseJSON(resp)
                 for ($i = 0  ; $i <= resp.length ; $i++  ) {
                    list[$i] = resp[$i].name ;
                }
                }
        });
        });
    });

    $('#textarea')
    .textext({
        plugins : 'tags autocomplete',
        tagsItems: [
            @if(isset($post->tags))
                    @foreach($post->tags as $tag)
                    '{{ $tag['name'] }}',
            @endforeach
            @endif
           ]
    })
    .bind('getSuggestions', function(e, data)
    {
        list,
        textext = $(e.target).textext()[0],
        query = (data ? data.query : '') || '';

        $(this).trigger(
            'setSuggestions',
            { result : textext.itemManager().filter(list, query) }
            );
    })
    ;
</script>

