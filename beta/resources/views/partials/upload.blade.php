{!! HTML::script('assets/js/app/Uploader.min.js') !!}
<script>
    $('.image_upload').on('click' , function(){
        $('input[name="uploadfile-Media"]').trigger('click') ;
    })
</script>
<script>
        window.onload = function () {
            if(document.getElementById('uploadBtn-Video')) {
                var btn = document.getElementById('uploadBtn-Video'),
                        progressBar = document.getElementById('progressBar-Video'),
                        progressOuter = document.getElementById('progressOuter-Video'),
                        msgBox = document.getElementById('msgBox-Video');
                        progress = document.getElementById('progress');

                var uploader = new ss.SimpleUpload({
                    button: btn,
                    url: "{{route('upload.UploadFile')}}",
                    name: 'uploadfile-Media',
                    multipart: true,
                    hoverClass: 'hover',
                    focusClass: 'focus',
                    responseType: 'json',
                    allowedExtensions: ['mp4'],

                    onSubmit: function () {
                        msgBox.innerHTML = ''; // empty the message box
                        document.getElementById('uploadBtn-Video').innerHTML = 'Uploading...'; // change button text to "Uploading..."
                    },
                    onComplete: function (filename, response) {
                        document.getElementById('uploadBtn-Video').innerHTML = 'Choose Another File';
                        progressOuter.style.display = 'none'; // hide progress bar when upload is completed

                        if (response.success === true) {
                            $('#respone-video').attr('style', 'display:auto ;width: 100%;');
                            $('#respone-video').attr('src', response.file_path);
                            $('#video-src').attr('value', response.file_where);
                            $('#mime').attr('value', response.mime);
                        } else {
                            if (response.msg) {
                                msgBox.innerHTML = escapeTags(response.msg);

                            } else {
                                msgBox.innerHTML = 'An error occurred and the upload failed.';
                            }
                        }
                    }
                });
            }
            if(document.getElementById('uploadBtn-Thumb')) {
                //Images Ajax Upload
                var btn = document.getElementById('uploadBtn-Thumb'),
                        progressBar = document.getElementById('progressBar-Thumb'),
                        progressOuter = document.getElementById('progressOuter-Thumb'),
                        msgBox = document.getElementById('msgBox-Thumb');

                var uploader = new ss.SimpleUpload({
                    button: btn,
                    url: "{{route('upload.UploadFile')}}",
                    name: 'uploadfile-Media',
                    multipart: true,
                    hoverClass: 'hover',
                    focusClass: 'focus',
                    responseType: 'json',
                    allowedExtensions: ['jpg', 'png'],

                    onSubmit: function () {
                        msgBox.innerHTML = ''; // empty the message box
                        document.getElementById('uploadBtn-Thumb').innerHTML = 'Uploading...'; // change button text to "Uploading..."
                    },
                    onComplete: function (filename, response) {
                        document.getElementById('uploadBtn-Thumb').innerHTML = 'Choose Another File';
                        progressOuter.style.display = 'none'; // hide progress bar when upload is completed

                        if (response.success === true) {

                            $url = "{{URL::to('/')}}/";
                            $('#respone-img').attr('style', 'display:auto ;width:100%;');
                            $('#respone-img').attr('src', response.file_path);
                            $('#thumb-src').attr('value', response.file_path);

                        } else {
                            if (response.msg) {
                                msgBox.innerHTML = escapeTags(response.msg);

                            } else {
                                msgBox.innerHTML = 'An error occurred and the upload failed.';
                            }
                        }
                    }
                });
            }
        };


</script>