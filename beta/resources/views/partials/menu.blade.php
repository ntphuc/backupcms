<aside class="left-panel">
    <div class="user text-center">
        {!! HTML::image('assets/images/avtar/user.png', 'phucnt' , ['class' => 'img-circle']) !!}
        <h4 class="user-name">Phucnt</h4>

        <div class="dropdown user-login">
            <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown"
                    aria-expanded="true">
                <i class="fa fa-circle status-icon available"></i> Available
            </button>
        </div>
    </div>
    {!! Menu::render('left_navbar') !!}
</aside>