@if($image_option == true )
    <div class="row" style="padding-top:10px;">
        <label class="col-sm-2 control-label">{{trans('content.upload_image')}} </label>
        <div class="col-xs-10">
            <button type="button" id="uploadBtn-Thumb"  style="" class="btn btn-danger btn-circle btn-flat"><span class="glyphicon glyphicon-send"></span>   Choose Image Upload </button>
        </div>
        <div class="col-xs-12">
            <div id="progressOuter-Thumb" class="progress progress-striped active" style="display:none;">
                <div id="progressBar-Thumb" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top:10px;">
        <div class="col-xs-12">
            <img  id="respone-img"  style=" width:100%;" src="@if(isset($data_post_thumbnail)){{$data_post_thumbnail}}@endif">
            <input type="hidden" value="@if(isset($data_post_thumbnail)){{$data_post_thumbnail}}@endif" name="media_thumbnail" id="thumb-src" required>
            <div id="msgBox-Thumb">
            </div>
        </div>
    </div>
    <hr class="dotted">
@endif

@if($video_option == true )
<div class="row" @if(\Request::segment(3) == 'new-post') style="padding-top:10px;display: none;" @else style="padding-top:10px;" @endif  >

    <label class="col-sm-2 control-label">{{trans('content.upload_video')}}</label>
    <div class="col-xs-10">
        <button type="button" id="uploadBtn-Video" class="btn btn-danger btn-circle btn-flat"><span class="glyphicon glyphicon-send"></span> Choose Video </button>
    </div>

    <input type="hidden" value="" name="media_path" id="video-src" required>
    <input type="hidden" value="-1" name="media_status">
    <input type="hidden" value="" name="media_mime_type" id="mime" required >
    <input type="hidden" value="" name="user_id">

    <div class="col-xs-12">
        <div id="progressOuter-Video" class="progress progress-striped active" style="display:none;">
            <div id="progressBar-Video" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
            </div>
        </div>
    </div>
    <label class="col-sm-2 control-label"></label>
    <div class="col-xs-7">
        <video id="respone-video" controls  @if(isset($data))src="{{env('link_static').str_replace(env('folder_remove') , '/' ,$data->media_path )}}" style="width:100%;"@else style="display: none ;width:100%;"@endif></video>
    </div>
</div>
<div class="row" style="padding-top:10px;">
    <div class="col-xs-12">
        <div id="msgBox-Video">
        </div>
    </div>
</div>

@endif


    
    
