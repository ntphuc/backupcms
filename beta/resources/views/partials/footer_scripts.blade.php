{!! HTML::script('assets/js/jquery/jquery-1.9.1.min.js') !!}

{!! HTML::script('assets/tinymce/tinymce.min.js') !!}

{!! HTML::script('assets/js/jquery.colorbox-min.js') !!}

{!! HTML::script('assets/js/plugins/underscore/underscore-min.js') !!}

{!! HTML::script('assets/js/bootstrap/bootstrap.min.js') !!}

{!! HTML::script('assets/js/globalize/globalize.min.js') !!}

{!! HTML::script('assets/js/plugins/pace/pace.min.js') !!}

{!! HTML::script('assets/js/plugins/nicescroll/jquery.nicescroll.min.js') !!}

{!! HTML::script('assets/js/plugins/sparkline/jquery.sparkline.min.js') !!}
{!! HTML::script('assets/js/plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('assets/js/plugins/datatables/DT_bootstrap.js') !!}
{!!  HTML::script('assets/js/extra/froala_editor.min.js') !!}
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/align.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/code_view.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/colors.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/font_size.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/font_family.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/image.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/file.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/link.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/lists.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/video.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/table.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/url.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/entities.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/save.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/extra/')}}/plugins/quote.min.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js/')}}/moment/moment.js"></script>
<script type="text/javascript" src="{{URL::to('/assets/js')}}/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
{!! HTML::script('assets/js/app/app.js') !!}
{!! HTML::script('assets/js/app/custom.js') !!}

<script>
    $(function () {

        $.FroalaEditor.DefineIcon('alert', {NAME: 'upload'});
        $.FroalaEditor.RegisterCommand('alert', {
            title: 'Quick Insert Video',
            focus: true,
            undo: false,
            refreshAfterCallback: false,
            callback: function () {
                $('#insert_video_button').trigger('click') ;
            }
        });
        $('#edit').froalaEditor({
            inlineMode: false,
            height: 400 ,
            linkAlwaysNoFollow: false,
            imageUploadURL: '{{ route('media.anyAjaxUploadImage')}}',
            imageManagerLoadURL: '{{ route('media.getAjaxLoadMedia')}}',
            imageUploadParams: {
                '_token' : "{{csrf_token()}}"
            },
            imageManagerLoadMethod: "GET",
            toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'fontFamily', 'fontSize', 'color', 'paragraphStyle', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', 'undo', 'redo', 'clearFormatting' ,'alert'],
            allowedBlankTags: ['placeholder']
        }) ;
        $('.edit').froalaEditor({
            inlineMode: false,
            height: 400 ,
            linkAlwaysNoFollow: false,
            imageUploadURL: '{{ route('media.anyAjaxUploadImage')}}',
            imageManagerLoadURL: '{{ route('media.getAjaxLoadMedia')}}',
            imageUploadParams: {
                '_token' : "{{csrf_token()}}"
            },
            imageManagerLoadMethod: "GET",
            toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'fontFamily', 'fontSize', 'color', 'paragraphStyle', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', 'undo', 'redo', 'clearFormatting' ,'alert'],
            allowedBlankTags: ['placeholder']
        }) ; 
    });
</script>
     <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
                $('#started_at').datetimepicker();
                $('#ended_at').datetimepicker();
            });
        </script>
<style>
    .fr-placeholder{
        display: none;
    }
</style>
<script type="text/javascript">
    function getval(sel) {
        window.location.replace(sel.value);
    }
</script>
<script>    // Check value input-number modul live
    $('.box-create-event .minutes').on('change', function(){
        if($(this).val() < 0) {
            $(this).val(0);
            alert('Minutes error!');
        }
    });
    $('.create-live .box-result, #editLive .box-result').on('change', ' .result', function(){
        if($(this).val() < 0) {
            $(this).val(0);
            alert('Minutes error!');
        }
    });
</script>
<script type="text/javascript">
            $("#check_all").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
</script>
<script>       //   Check notification
    setInterval(function(){
        $.ajax({
            url: '{{ route('Notification') }}',
            type: 'GET',
            success: function(resp) {
                if(resp == 'reload') {
                    location.reload();
                }
            }
        });
    }, 5000);
</script>

    
@include('popup.popup_insert_video_content')
@include('partials.inlcude_js.module_post')