<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PhpSpec' => array($vendorDir . '/phpspec/phpspec/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Jenssegers\\Mongodb\\Session' => array($vendorDir . '/jenssegers/mongodb-session/src'),
    'Jenssegers\\Mongodb\\Sentry' => array($vendorDir . '/jenssegers/mongodb-sentry/src'),
    'Jenssegers\\Mongodb' => array($vendorDir . '/jenssegers/mongodb/src'),
    'Jenssegers\\Eloquent' => array($vendorDir . '/jenssegers/mongodb/src'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Dotenv' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Cron' => array($vendorDir . '/mtdowling/cron-expression/src'),
    'Cartalyst\\Sentry' => array($vendorDir . '/cartalyst/sentry/src'),
);
