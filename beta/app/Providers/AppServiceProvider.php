<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\UploadHandler as UploadMedia ;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('UploadMedia', function () {
            return new UploadMedia('uploadfile-Media');
        });
        $this->app->singleton('Post', function () {
            return new \App\Models\Post();
        });
        $this->app->singleton('PostMeta', function () {
            return new \App\Models\PostMeta();
        });
        $this->app->singleton('Film', function () {
            return new \App\Models\Film();
        });
        $this->app->bind('App\Repositories\Post\PostRepository', 'App\Repositories\Post\EloquentPost');
        $this->app->bind('App\Repositories\Media\MediaRepository', 'App\Repositories\Media\EloquentMedia');
        $this->app->bind('App\Repositories\Tag\TagRepository', 'App\Repositories\Tag\EloquentTag');
        $this->app->bind('App\Repositories\Category\CategoryRepository', 'App\Repositories\Category\EloquentCategory');
        $this->app->bind('App\Repositories\Live\LiveRepository', 'App\Repositories\Live\EloquentLive');
        $this->app->bind('App\Repositories\Sms\SmsRepository', 'App\Repositories\Sms\EloquentSms') ; 
    }
}
