<?php
namespace App\Repositories\Sms ;
use App\Models\Sms15602 ; 
class EloquentSms implements  SmsRepository {
    public function  getSms ($short_code) {
        $model = '\App\Models\Sms'.$short_code ;
        return $model::orderBy('created_at', 'desc')->paginate(20) ;
        
    }
    public function InsertSms( $short_code  , $array )   {
        $model = '\App\Models\Sms'.$short_code  ;
        $sms = new $model() ; 
        foreach($array as $k => $v ) {
            $sms->$k = $v ;
        }
        //$sms->save() ; 
        
    }
    public function getSmsbyID($short_code , $id ) {
        $model = '\App\Models\Sms'.$short_code  ;
        return  $model::find($id) ; 
    }
    public function EditSms ($short_code , $array , $id )  {
        $model = '\App\Models\Sms'.$short_code  ;
        $sms = $model::find($id) ; 
        foreach($array as $k => $v ) {
            $sms->$k = $v ;
        }
        //$sms->save() ;
        
    }
    public function DeleteSms ($short_code , $id ) {
        $model = '\App\Models\Sms'.$short_code  ;
        $sms = $model::find($id) ;
        //if($sms->delete()){
        //    return true ; 
        //}else {
        //    return false ; 
        //}
    }
    public function GetService($short_code) {
        $model = '\App\Models\Sms'.$short_code ;
        return $model::getService() ; 
    }
    public function GetProvider($short_code) {
        $model = '\App\Models\Sms'.$short_code ;
        return $model::getProvider() ; 
    }
}
