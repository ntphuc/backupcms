<?php
namespace App\Repositories\Sms ;
interface SmsRepository {
    public function getSms($short_code  ) ;
    public function InsertSms( $short_code  , $array )  ;
    public function EditSms ($short_code , $array , $id ) ;
    public function DeleteSms ($short_code , $id ) ;
    public function GetService ($short_code) ;
    public function GetProvider ($short_code) ;
}
