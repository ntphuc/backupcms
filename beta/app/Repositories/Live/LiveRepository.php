<?php
/**
 * Created by PhpStorm.
 * User: phucnt
 * Date: 18/12/2015
 * Time: 13:14
 */
namespace App\Repositories\Live;

interface LiveRepository
{
    public function getAllLive($page);

    public function getLivebyCategory();

    public function getLivePaginate($array, $paginate);

    public function getLiveWhere($where, $take, $order_by);

    public function UpdateLive($id, $array);

    public function DeleteLive($id);

    public function InsertEvent($list_id, $array);

    public function UpdateEvent($live_id,$event_key,$events);

    public function DeleteEvent($live_id, $event_key);

    public function InsertLive($array);

    public function findLivebyId($id);

    public function getSearch($keyword, $page);

    public function live($id);

    public function disLive($id);

}