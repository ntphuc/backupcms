<?php
/**
 * Created by PhpStorm.
 * User: phucnt
 * Date: 18/12/2015
 * Time: 13:13
 */
namespace App\Repositories\Live ;

use App\Models\LiveSchedule as Live;

class EloquentLive implements LiveRepository {

    function getAllLive($page = 25)
    {
        $list_live = Live::orderBy('created_at','desc')->paginate((int)$page);
        if($list_live != null) {
            return $list_live;
        }
        return false;
    }
    function getLivebyCategory()
    {
        // TODO: Implement getLivebyCategory() method.
    }
    function getLivePaginate($array, $paginate)
    {
        // TODO: Implement getLivePaginate() method.
    }
    function getLiveWhere($where, $take, $order_by)
    {
        // TODO: Implement getLiveWhere() method.
    }

    function UpdateLive($id, $array)
    {
        $live = Live::find($id);
        if($live != null) {
            foreach($array as $key => $value) {
                $live->$key = $value;
            }
            if($live->save()) {
                return true;
            }
        }
        return false;
    }
    
    function DeleteLive($id)
    {
        $live = Live::find($id);
        if($live != null) {
            if($live->delete()) {
                return true;
            }
        }
        return false;
    }

    function InsertEvent($live_id, $array) {

        $live = Live::find($live_id);

        if($live != null) {
            if(!isset($live->events)) {
                $live->events = array();
            }
            $events = $live->events;
            foreach($events as $event) {    //  Kiem tra phut do da co live chua?
                if($event['minutes'] == $array['minutes']) return false;
                if($array['type'] == 'fulltime') return false;
            }
            $events[$array['minutes']] = $array;
            krsort($events);
            $live->events = $events;

            //  Cap nhat ti so tran dau
            if(!empty($live->result)) {
                $result = $live->result;
            } else {
                $result['goalsHomeTeam'] = $result['goalsAwayTeam'] = 0;
            }
            if($array['type'] == 'goal' || $array['type'] == 'pen') {  // Neu la ban thang thi cap nhat ti so
                if($array['event_of_team'] == 'home') {
                    $result['goalsHomeTeam'] ++;
                } else {
                    $result['goalsAwayTeam'] ++;
                }
                $live->result = $result;
            } elseif($array['type'] == 'og_goal') { // Neu phan luoi nha
                if($array['event_of_team'] == 'home') {
                    $result['goalsAwayTeam'] ++;
                } else {
                    $result['goalsHomeTeam'] ++;
                }
                $live->result = $result;
            }

            // Cap nhat status
            if($array['type'] == 'fulltime') {
                $live->status = 'FINISHED';
            }
            $live->have_report = true;  // Cap nhat live tran dau
            if($live->save()) {
                return true;
            }
        }
        return false;
    }

    public function UpdateEvent($live_id,$event_key,$event) {

        $live = Live::find($live_id);

        if($live != null) {
            if(isset($live->events[$event_key])) {
                // Update events
                $events = $live->events;
                // if update goal
                $goal = array('goal', 'pen');
                $miss_goal = array('text', 'miss_pen', 'red_card', 'yellow_card', '2yellow_card', 'fulltime');
                if(in_array($live->events[$event_key]['type'], $goal)){
                    if(in_array($event['type'], $miss_goal)) {    // goal => miss_goal
                        if($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        }
                    } elseif(in_array($event['type'], $goal)) {     // goal => goal
                        if($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        }
                    } elseif($event['type'] == 'og_goal') {     // goal => og_goal
                        if($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        }
                    }
                } elseif(in_array($live->events[$event_key]['type'], $miss_goal)){
                    if(in_array($event['type'], $goal)) {    // miss_goal => goal
                        if($event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam']  ] ;
                        } else {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        }
                    } elseif($event['type'] == 'og_goal') {     // miss_goal => og_goal
                        if($event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        } else {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] ] ;
                        }
                    }
                } elseif($live->events[$event_key]['type'] == 'og_goal') {
                    if(in_array($event['type'], $goal)) {    // og_goal => goal
                        if($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        }
                    } elseif(in_array($event['type'], $miss_goal)) {     // og_goal => miss_goal
                        if($live->events[$event_key]['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam']  ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        } else {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam']  ] ;
                        }
                    } elseif($event['type'] == 'og_goal') {     // og_goal => og_goal
                        if($live->events[$event_key]['event_of_team'] == 'home' && $event['event_of_team'] == 'away') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] + 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                        } elseif($live->events[$event_key]['event_of_team'] == 'away' && $event['event_of_team'] == 'home') {
                            $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] + 1 ] ;
                        }
                    }
                }
                if($event_key != $event['minutes']) {   // If change minutes
                    if(isset($live->events[$event['minutes']])) {        // Neu update bi trung event_key
                        return false;
                    }
                    unset($events[$event_key]);         // Remove old element
                    $events[$event['minutes']] = $event;// Add new element
                    krsort($events);
                } else {
                    $events[$event_key] = $event;
                }
                $live->events = $events;
                if($live->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    public function DeleteEvent($live_id, $event_key) {

        $live = Live::find($live_id);

        if($live != null) {
            if(isset($live->events[$event_key])) {
                $events = $live->events;
                if($events[$event_key]['type'] == 'goal' || $events[$event_key]['type'] == 'pen') { // Neu xoa event goal thi update ti so
                    if($events[$event_key]['event_of_team'] == 'home') {
                        $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam']  ] ;
                    } else {
                        $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                    }
                } elseif($events[$event_key]['type'] == 'og_goal') { // Neu xoa event og_goal thi update ti so
                    if($events[$event_key]['event_of_team'] == 'home') {
                        $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] - 1 ] ;
                    } else {
                        $live->result =['goalsHomeTeam' => $live->result['goalsHomeTeam'] - 1 ,'goalsAwayTeam' => $live->result['goalsAwayTeam'] ] ;
                    }
                }

                if($events[$event_key]['type'] == 'fulltime') {
                    $live->status = 'TIMED';
                }
                unset($events[$event_key]);
                $live->events = $events;
                if($live->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    function InsertLive($array)
    {
        $live = new Live();
        foreach($array as $key => $value) {
            $live->$key = $value;
        }
        if($live->save()) {
            return $live;
        }
        return false;
    }

    public function findLivebyId($id) {
        $live = Live::find($id);
        if($live != null) {
            return $live;
        }
        return false;
    }

    public function getSearch($keyword, $page) {
        $keyword= preg_replace('/\s\s+/', ' ', trim($keyword));
        $this->key_word = $keyword ;
        $list_live = Live::orderBy('created_at','desc')->where(function($query) {
            $query->where('homeTeamName', 'regexp', '/'. $this->key_word . '/i' );
            $query->orWhere('awayTeamName', 'regexp', '/'. $this->key_word.'/i');
        })->paginate((int)$page);
        if($list_live != null) {
            return $list_live;
        }
        return false;
    }

    public function live($id){
        $live = Live::find($id);
        if($live != null) {
            $live->have_report = true;
            $live->result =['goalsHomeTeam' => 0 ,'goalsAwayTeam' => 0 ] ;
            if($live->save()) {
                return true;
            }
        }
        return false;
    }

    public function disLive($id){
        $live = Live::find($id);
        if($live != null) {
            $live->have_report = false;
            if($live->save()) {
                return true;
            }
        }
        return false;
    }
}