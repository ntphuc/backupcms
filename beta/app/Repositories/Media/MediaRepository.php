<?php

namespace App\Repositories\Media;
interface MediaRepository
{
    public function getAllMedia($page, $user);

    public function findMediabyId($id);

    public function UpdateMedia($id, $array);

    public function DeleteMedia($id);

    public function InsertMedia($array);

    public function getSearch($keyword, $page , $user );

    public function StatusMedia($id, $status);


}

