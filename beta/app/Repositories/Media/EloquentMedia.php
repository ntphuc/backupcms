<?php

namespace App\Repositories\Media;

use App\Models\Media;

class EloquentMedia implements MediaRepository
{
    function getAllMedia($page, $user)
    {
        /*if(Media::paginate((int)$page)){
            return Media::paginate((int)$page);
        }
        return false;*/
        if (Media::paginate((int)$page)) {
            if ($user == 'all') {
                return Media::orderBy('created_at', 'desc')->paginate((int)$page);
            } else {
                return Media::orderBy('created_at', 'desc')->where('user_id', '=', $user)->paginate((int)$page);
            }
        }
        return false;
    }
    function findMediabyId($id)
    {
        if (Media::find($id)) {
            return Media::find($id);
        } else {
            return false;
        }
    }
    function InsertMedia($array)
    {
        $media = new Media();
        foreach($array as $k => $v) {
            if($k == 'media_status'){
                $media->$k = (int)$v;
            }
            else{
                $media->$k = $v;
            }
        }
        if ($media->save()) {
            return true;
        }
        else{
            return false;
        }
    }
    function UpdateMedia($id, $array)
    {
        $media = Media::find($id);
        if($media){
            foreach ($array as $k => $v) {
                $media->$k = $v;
            }
            if($media->save()){
                return true;
            }else{
                return false;
            }
        }
    }
    public function StatusMedia($id, $status)
    {
        $media = Media::find($id);
        if($media){
            $media->media_status = $status;
            $media->save();
        }else{
            return false;
        }
    }
    function DeleteMedia($id)
    {
        $media = Media::find($id);
        if($media){
            $media->delete();
        }
        return false;
    }
    public function getSearch($keyword, $page, $user)
    {
        if($user != 'all'){
            $media = Media::where('media_name', 'regexp', '/' . $keyword . '/i,x')
                ->where('user_id' , '=' , $user)
                ->orderBy('created_at', 'desc')
                ->paginate((int)$page);
        }else{
            $media = Media::where('media_name', 'regexp', '/' . $keyword . '/i,x')
                ->orderBy('created_at', 'desc')
                ->paginate((int)$page);
        }
        return $media;
    }


}

