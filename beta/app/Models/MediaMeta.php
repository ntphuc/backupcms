<?php

namespace App\Models;

use Jenssegers\Mongodb\Model  ;

class MediaMeta extends Model
{
    protected $collection = 'media_meta';

    /**
     * Inverse Relation
     */
    use MediaMetaBelongToMedia;
}
