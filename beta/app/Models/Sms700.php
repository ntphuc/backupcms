<?php
namespace App\Models ;
use Illuminate\Database\Eloquent\Model;
class Sms700 extends Model {
    protected $connection = 'sms_700' ;
    protected $table = 'sms_mt_content'  ;
    static function getService(){
        $connection = 'sms_700' ; 
        return \DB::connection('sms_700')->table('services')->get();
    }
    static function getProvider(){
        $connection = 'sms_700' ; 
        return \DB::connection('sms_700')->table('provider')->get();
    }
}