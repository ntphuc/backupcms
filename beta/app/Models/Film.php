<?php

namespace App\Models;

use Jenssegers\Mongodb\Model  ;

class Film extends Model
{
    protected $collection = 'films';

    /**
     * Relationships
     */

    protected $fillable = [];

    protected $hidden = [];
}
