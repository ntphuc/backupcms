<?php
/**
 * Created by PhpStorm.
 * User: ger2017
 * Date: 11/13/15
 * Time: 6:53 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Model  ;

class Category extends Model
{
    protected $collection = 'category';

    /**
     * Relationships
     */
    /**
     * Inverse of Relation
     */

    protected $fillable = [];

    protected $hidden = [];

}