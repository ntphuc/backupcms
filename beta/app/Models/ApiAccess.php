<?php
/**
 * Created by PhpStorm.
 * User: ger2017
 * Date: 11/26/15
 * Time: 10:47 AM
 */

namespace App\Models;


use Jenssegers\Mongodb\Model  ;

class ApiAccess extends Model
{
    protected $collection = 'api_access';
}