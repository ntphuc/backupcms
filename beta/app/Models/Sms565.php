<?php
namespace App\Models ;
use Illuminate\Database\Eloquent\Model;
class Sms565 extends Model {
    protected $connection = 'sms_565' ;
    protected $table = 'sms_mt_content'  ;
    
    static function getService(){
        $connection = 'sms_565' ; 
        return \DB::connection('sms_565')->table('services')->get();
    }
    static function getProvider(){
        $connection = 'sms_565' ; 
        return \DB::connection('sms_565')->table('provider')->get();
    }
}