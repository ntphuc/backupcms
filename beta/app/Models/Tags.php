<?php
/**
 * Created by PhpStorm.
 * User: ger2017
 * Date: 11/13/15
 * Time: 6:51 PM
 */

namespace App\Models;


use Jenssegers\Mongodb\Model  ;
class Tags extends Model
{
    protected $collection = 'tags';


    /**
     * Relationships
     */

    protected $fillable = [];

    protected $hidden = [];
}