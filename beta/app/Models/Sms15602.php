<?php
namespace App\Models ;
use Illuminate\Database\Eloquent\Model;
class Sms15602 extends Model {
    protected $connection = 'sms_15602' ;
    protected $table = 'sms_mt_content'  ;
    static function getService(){
        $connection = 'sms_15602' ; 
        return \DB::connection('sms_15602')->table('services')->get();
    }
    static function getProvider(){
        $connection = 'sms_15602' ; 
        return \DB::connection('sms_15602')->table('provider')->get();
    }
}