<?php

namespace App\Models;

use Jenssegers\Mongodb\Model  ;

class Media extends Model
{

    protected $collection = 'medias';

    /**
     * Relationships
     */

    protected $fillable = [];

    protected $hidden = [];
}
