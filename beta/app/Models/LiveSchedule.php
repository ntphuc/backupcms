<?php

namespace App\Models;

use Jenssegers\Mongodb\Model  ;

class LiveSchedule extends Model
{
    protected $collection = 'fixtures';


    /**
     * Relationships
     */

    protected $fillable = ['homeTeamName', 'homeTeamLogo', 'awayTeamName', 'awayTeamLogo', 'result', 'have_report', 'description'];

    protected $hidden = [];
}
