<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('/', 'DashBoardController@getIndex');

    Route::controller('live', 'LiveController', [
        'getIndex' => 'live',
        'anyLiveData' => 'live.anyLiveData',
        'anyActionLive' => 'live.ActionLive',
        'anyViewListLive' => 'live.ListLive',
        'getCreate' => 'live.getCreate',
        'postCreate' => 'live.postCreate',
        'getEdit' => 'live.getEdit',
        'postEdit' => 'live.postEdit',
        'getActive' => 'live.getActive',
        'getEvents' => 'live.getEvents',
        'postEditEvents'=> 'live.postEditEvents',
        'postDeleteEvents'=> 'live.postDeleteEvents',
        'anyEventData' => 'live.anyEventData',
        'postEventCreate' => 'live.postEventCreate',
        'postUpdateEvent' => 'live.postUpdateEvent',
        'getDeleteEvent' => 'live.getDeleteEvent',
        'getTableReviewLive' => 'live.getTableReviewLive'
    ]);

    Route::controller('media', 'MediaController', [
        'getIndex' => 'media',
        'anyAction' => 'media.anyAction',
        'getUpdate' => 'media.getUpdate',
        'postProcessMedia' => 'media.ProccessMedia',
        'anyActionMedia' => 'media.ActionMedia',
        'anyViewListMedia' => 'media.ListMedia',
        'getEditMedia' => 'media.EditMedia',
        'postUpdate' => 'media.postUpdate'
    ]);
    Route::resource('api-v1' , 'ApiController' , [
        'store' => 'RestApi'
    ]) ; 

    Route::controller('post', 'PostController', [
        'getIndex' => 'post',
        'getEditPost' => 'post.editPost',
        'getReviewPost' => 'ReviewPost',
        'getUpdateStatusPost' =>  'UpdateStatusPost' , 
        'getCreatePost' => 'post.getCreate',
        'postProcessPost' => 'post.postCreate',
        'postTag' => 'post.getTag/{slug}',
        'getSlug' => 'post.getSlug',
        'getReview' => 'post.getReview',
        'anyActionPost' => 'post.ActionPost' ,

    ]);
    Route::controller ( 'sms' , 'SmsController' ,  [
        'getIndex' => 'sms'  ,
        'getCreateSms' => 'send_sms' ,
        'postCreateSms' => 'sending_sms'  ,
        'getEditSms' => 'fix_sms'  ,
        'postEditSms' => 'fixing_sms'  ,
        'postDeleteSms' => 'delete_sms'
        
    ]) ;
    
    Route::controller('queue' , 'QueueController' , [
        'anyIndex' => 'ReleasePushToQueue'  ,
        'anyNotification' => 'Notification'
    ]) ; 
});
Route::controller('upload', 'UploadController', [
    'anyUploadFile' => 'upload.UploadFile',
    'getAjaxLoadImage' => 'media.getAjaxLoadMedia',
    'anyAjaxUploadImage' => 'media.anyAjaxUploadImage', 
]);
