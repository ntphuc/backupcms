<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Lang;
use Menu;

class MenuMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('left_navbar', function ($menu) {
            $menu->style('navigation');
            $menu->add([
                'url' => 'dashboard',
                'title' => 'Dashboard',
                'icon' => 'fa fa-desktop'
            ]);
            $menu->dropdown('Digitizing', function ($sub) {
                $sub->url(route('media'), 'Media');
                $sub->url(route('live'), 'Livescore');
            }, '', ['icon' => 'fa fa-cogs']);
            $menu->add([
                'url' => route('sms'),
                'title' => 'SMS Platform',
                'icon' => 'fa fa-envelope-o'
            ]);
            $menu->dropdown('Post', function ($sub) {
                $sub->url(route('post'), 'View Posts');
                $sub->url(route('post.getCreate'), 'Create New Post');
            }, '', ['icon' => 'fa  fa-file-word-o']);
            $menu->dropdown('Report', function ($sub) {
                $sub->url(route('post'), 'Content Report');
                $sub->url(route('post.getCreate'), 'SMS Report');
            }, '', ['icon' => 'fa  fa-line-chart']);
            $menu->add([
                'url' => '',
                'title' => 'Users',
                'icon' => 'glyphicon glyphicon-user'
            ]);
            $menu->add([
                'url' => '',
                'title' => 'Config',
                'icon' => 'fa fa-cog'
            ]);
            

//            $menu->dropdown(trans('menu.sms'), function ($sub) {
//                $sub->url(route('user.getProfile'), trans('menu.list_sms'));
//                $sub->url(route('user'), trans('menu.create_sms_content'));
//            }, '', ['icon' => 'fa fa-envelope-o']);
//
//            $menu->add([
//                'url' => route('option'),
//                'title' => trans('menu.post'),
//                'icon' => 'fa fa-code'
//            ]);
//
//            $menu->add([
//                'url' => route('live'),
//                'title' => trans('menu.live'),
//                'icon' => 'fa fa-calendar'
//            ]);
//
//            $menu->add([
//                'url' => route('film'),
//                'title' => trans('menu.film'),
//                'icon' => 'fa fa-film'
//            ]);
//
//
//            $menu->dropdown(trans('menu.post'), function ($sub) {
//                $sub->url(\URL::to('/dashboard/post/new-posts'), trans('menu.new-post'));
//
//            }, '', ['icon' => 'fa fa-align-justify']);
//
//            $menu->add([
//                'url' => route('category'),
//                'title' => trans('menu.category'),
//                'icon' => 'fa fa-bookmark'
//            ]);
//
//            $menu->add([
//                'url' => route('option'),
//                'title' => trans('menu.option'),
//                'icon' => 'fa fa-gear'
//            ]);
//
//            $menu->dropdown(trans('menu.report'), function ($sub) {
//                $sub->url('', trans('menu.profile'));
//                $sub->url('', trans('menu.list_user'));
//            }, '', ['icon' => 'fa fa-bar-chart-o']);
//
//            $menu->add([
//                'url' => route('user'),
//                'title' => trans('menu.user'),
//                'icon' => 'fa fa-user'
//            ]);
        });
        return $next($request);
    }
}
