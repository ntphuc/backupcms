<?php
namespace App\Http\Controllers;

use App\Repositories\Sms\SmsRepository;
class SmsController extends Controller {
    
    private $short_code = ['15602' , '700' , '565'] ;
    private $service_name = ['15602' => 'HALOSOKA-TRAITHIVANG'  ,'700' => 'KQTT-TRAITHIVANG' , '565' => 'TUYENGAP-TRAITHIVANG' ] ;
    
    function __construct (SmsRepository $sms ) {
        $this->_sms = $sms ; 
    }
    function getIndex() {
        $short_code = \Input::get('short_code') ;
        if(!in_array($short_code , $this->short_code) ) {
            return view('child.sms.choose_service_sms')   ; 
        }else{
            if($short_code == null ) {
                return \Redirect::to('/')    ; 
            }else {
                $data['all_sms'] = $this->_sms->getSms( $short_code ) ;
                $data['service_name'] = $this->service_name[$short_code] ; 
                $data['short_code'] = $short_code  ;
                return view('child.sms.list_sms')->with($data)  ; 
            }    
        }
        
    }
    function getCreateSms(){
        $data['short_code'] =   \Input::get('short_code') ;
        $data['services'] = $this->_sms->GetService($data['short_code']) ;
        $data['providers'] = $this->_sms->GetProvider($data['short_code']) ;  
        return view('child.sms.new_sms')->with($data)  ; 
    }
    function postCreateSms(){
        $input = \Input::except('_token' , 'forward') ;
        if(\Input::has('forward') &&  \Input::get('forward') == 'on'){
            $input['started_at'] = $input['started_at'] ; 
        }else {
            $input['started_at'] = date('Y-m-d H:i:s' , strtotime(date('Y-m-d H:i:s')) + 900 ) ; 
        }
        $input['content'] = str_replace('\\' , PHP_EOL , $input['content']) ;
        if($this->_sms->InsertSms( \Input::get('short_code')  , $input ) ){
            \Session::flash('success' , 'Sended Sms') ; 
            return Redirect::to(route('sms').'?short_code='.\Input::get('short_code')) ; 
        }else{
            \Session::flash('error' , 'Cannot Send Sms') ; 
            return Redirect::to(route('sms').'?short_code='.\Input::get('short_code')) ; 
        };
        
    }
    function getEditSms($id){
        $data['short_code'] =   \Input::get('short_code') ;
        $content =  $this->_sms->getSmsbyID($data['short_code'] , $id ) ;
        $data['content'] =  $content ; 
        if(strtotime(date('Y-m-d H:i:s')) < strtotime(date('Y-m-d H:i:s' ,strtotime($content->started_at)))) {
            $data['services'] = $this->_sms->GetService($data['short_code']) ;
            $data['providers'] = $this->_sms->GetProvider($data['short_code']) ;
            return view('child.sms.edit_sms')->with($data)  ;     
        }else {
            \Session::flash('error' , 'You cannot edit it !') ; 
            return \Redirect::back();
        }
    }
    function postEditSms(){
        $input = \Input::except('_token' , 'id' , 'forward') ;
        if(\Input::has('forward') &&  \Input::get('forward') == 'on'){
            $input['started_at'] = $input['started_at'] ; 
        }else {
            $input['started_at'] = date('Y-m-d H:i:s' , strtotime(date('Y-m-d H:i:s')) + 900 ) ; 
        }
        $input['content'] = str_replace('\\' , PHP_EOL , $input['content']) ;
        $this->_sms->EditSms ($input['short_code'] , $input , \Input::get('id') )  ; 
        \Session::flash('success' , 'Fixed Done !' ) ; 
        return \Redirect::back() ;   
        
        
    }
    function postDeleteSms(){
        $ids_delete = \Input::get('listId') ;
        if(!empty($ids_delete)) {
            foreach($ids_delete as $items ) {
                $this->_sms->DeleteSms (\Input::get('short_code') , $items ) ; 
            }
            \Session::flash('success' , 'Deleted Done !' ) ; 
            return \Redirect::back() ;     
        }else {
            \Session::flash('error' , 'Cannot Delete!' ) ; 
            return \Redirect::back() ;     
        }
    }
}