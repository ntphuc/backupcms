<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Events\SendingMail  ; 
use App\Repositories\Media\MediaRepository;
use Illuminate\Support\Facades\App;
use App\Helpers\FroalaHelper;


class MediaController extends Controller
{

    function __construct(MediaRepository $media)
    {
        $this->media = $media;
    }
    function getNum () {

        \Event::listen('auth.login', function($user)
        {
            echo $user ; 
        });
        \Event::fire('auth.login' , array($user = 1 ) );

    }
    function getIndex()
    {
        \Session::has('media_per_page') ? $page = \Session::get('media_per_page') : $page = 25;
        \Session::has('media_by_user') ? $user = \Session::get('media_by_user') : $user = 'all';
        \Input::has('q') ? $data['media_datas'] = $this->media->getSearch(\Input::get('q'), $page , $user ) : $data['media_datas'] = $this->media->getAllMedia($page ,$user);
        if (\Input::get('popup') != 'true') {
            return view('child.media.listmedia')->with($data);
        } else {
            return view('child.media.listmedia_popup')->with($data);
        }
    }
    function getCreate()
    {
        return view('child.media.upload');
    }
    function postCreate()
    {
        $input = \Input::except('_token');
        if ($this->media->InsertMedia($input) === true) {
            \Session::flash('success', 'Upload File Success');
            return redirect('dashboard/media');
        } else {
            \Session::flash('error', 'Wrong When You Upload File ');
            return redirect('dashboard/media/create');
        }
    }
    function getUpdate($id)
    {
        if ($this->media->findMediabyId($id) != false) {
            $video = $this->media->findMediabyId($id);
            $data['media_data'] = $video;
            return view('child.media.edit')->with($data);
        } else {
            \Session::flash('error', 'Not Found');
            return \Redirect::to('dashboard/media');
        }
    }
    function postUpdate()
    {
        $id = \Input::get('id') ;
        if($this->media->findMediabyId($id) != false){
            $input = \Input::except('_token');
            if($this->media->UpdateMedia($id, $input)){
                \Session::flash('success', 'Update Media Success');
                return redirect('dashboard/media');
            } else {
                \Session::flash('error', 'Wrong When You Update Media ');
                return redirect('dashboard/media/update');
            }
        }
    }
    // Action Delete, Active, Deactive Media
    public function anyAction()
    {
        if (!\Input::has('session') && !\Input::has('user') ) {
            $check_media = \Input::get('listId');
            $action_media = \Input::get('select_action');
            if(isset($check_media)) {
                foreach ($check_media as $id) {
                    if (!empty($check_media) && $action_media) {
                        if ($action_media == 'Delete') {
                            $this->media->DeleteMedia($id);
                            \Session::flash('success', 'Delete Video Success');
                        }
                        if ($action_media == 'Active') {
                            $this->media->StatusMedia($id, 1);
                            \Session::flash('success', 'Active File Success');
                        }
                        if ($action_media == 'Deactive') {
                            $this->media->StatusMedia($id, 0);
                            \Session::flash('success', 'Deactive File Success');
                        }
                    }
                }
            }
            return \Redirect::back();
        } else {
            if (!\Input::has('user')) {
                \Session::put('media_per_page', \Input::get('session'));
            } else {
                if(\Input::get('user') != 'all') {
                    \Session::put('media_by_user', \Input::get('user'));
                }else{
                    \Session::forget('media_by_user');
                }
            }
        }
    }
}

