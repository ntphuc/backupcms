<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\Tag\TagRepository;
use Illuminate\Http\Request;
use App\Jobs\ActionContent;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{

    protected $_category_model;
    protected $_post_model;
    protected $_tag_model;

    public $post_status_default = 'draft';
    public $tag_match_order = ['col' => 'name', 'mode' => 'ASC'];
    public $tag_match_limit = 4;

    public function __construct(CategoryRepository $category, PostRepository $post, TagRepository $tag)
    {
        $this->_category_model = $category;
        $this->_post_model = $post;
        $this->_tag_model = $tag;
    }

    public function getCreatePost()
    {
        return view('child.post.newpost');
    }

    public function postProcessPost(Request $request)
    {
        $params = $request->except(['_token', 'post_thumbnail', 'meta_title', 'meta_keywords', 'meta_description']);

        // Get value Title (post_name)
        $params['post_name'] = $request->post_name;

        // Get value slug_url
        $params['slug_url'] = str_slug($request->post_name, "-") . ".html";
        $posts = $this->_post_model->getAllPost();
        foreach ($posts as $key => $post) {
            if ($post->slug_url == $params['slug_url']) {
                $params['slug_url'] = str_slug($request->post_name, "-") . uniqid() . ".html";
            }
        }

        // Get value Description
        $params['post_description'] = $request->post_description;

        // Get value Content
        $params['post_content'] = $request->post_content;

        // Get value post_thumbnail(anh dai dien)
        $params['post_thumbnail'] = $request->media_thumbnail;

        // Get value Status
        $params['post_status'] = 0;

        // Get Thong ke luot xem
        $params['viewers'] = array(
            'today' => 0,
            'this_week' => 0,
            'this_month' => 0,
            'total' => 0
        );

        // Get value Tags
        //  Cover tags(string => array)
        if ($request->tags) {
            $tag_field = array();
            $tags = json_decode($request->tags);
            $i = 0;     // SET key for array tags
            foreach ($tags as $tag) {
                $tag_result = $this->_tag_model->getTagbyTagName($tag);
                if ($tag_result) {   // if exist tag in table tags
                    $tag_field[$i]['name'] = $tag_result->name;
                    $tag_field[$i]['uri'] = $tag_result->slug_url;
                } else {  //  if not exist tag in table tags
                    $tag_new['name'] = $tag_field[$i]['name'] = $tag;
                    $tag_new['slug_url'] = $tag_field[$i]['uri'] = str_slug($tag, "-");
                    // create new tag in table tags
                    $this->_tag_model->InsertTag($tag_new);
                }
                $params['tags'] = $tag_field;
                $i++;
            }
        }

        // GET field SEO
        $params['seo'] = array();

        if ($request->meta_title != null) {
            $params['seo']['title'] = $request->meta_title;
        } else {
            $params['seo']['title'] = $request->post_name;
        }

        if ($request->meta_description != null) {
            $params['seo']['description'] = $request->meta_description;
        } else {
            $params['seo']['description'] = $request->post_name;
        }
        if ($request->meta_keywords != null) {
            $params['seo']['meta'] = $request->meta_keywords;
        } else {
            $params['seo']['meta'] = $request->post_name;
        }

        // Get User_id
        //$params['user_id'] = '569867003d1138582e8b4567';

        // Get value lang code
        $params['lang_code'] = $request->lang_code;

        // Get value category
        $params['category_id'] = $request->category;

        if ($this->_post_model->InsertPost($params)) {
            \Session::flash('success', 'Created Success !');
            return Redirect('dashboard/post');
        } else {
            \Session::flash('error', 'Error!');
            return Redirect('dashboard/post/newpost');
        }
    }


    public function getEditPost($post_id)
    {
        // kiem tra ton tai chua.
        $post = $this->_post_model->findPostbyId($post_id);
        if ($post == false) {
            abort(404);
        }
        return view('child.post.editpost')->with('post', $post);
    }

    public function postEditPost(Request $request)
    {
        $params = $request->except(['_token', 'post_id', 'post_thumbnail', 'meta_title', 'meta_keywords', 'meta_description']);

        // Get value Title (post_name)
        $params['post_name'] = $request->post_name;

        // Get value slug_url
        $post = $this->_post_model->findPostbyId($request->post_id);
        if ($post->post_name == $params['post_name']) {
            $params['slug_url'] = $post->slug_url;
        } else {
            $params['slug_url'] = str_slug($request->post_name, "-") . ".html";
            $posts = $this->_post_model->getAllPost();
            foreach ($posts as $key => $post) {
                if ($post->slug_url == $params['slug_url']) {
                    $params['slug_url'] = str_slug($request->post_name, "-") . uniqid() . ".html";
                }
            }
        }

        // Get value Description
        $params['post_description'] = $request->post_description;

        // Get value Content
        $params['post_content'] = $request->post_content;

        // Get value post_thumbnail(anh dai dien)
        $params['post_thumbnail'] = $request->media_thumbnail;

        // Get value Tags
        //  Cover tags(string => array)
        if ($request->tags) {
            $tag_field = array();
            $tags = json_decode($request->tags);
            $i = 0;     // SET key for array tags
            foreach ($tags as $tag) {
                $tag_result = $this->_tag_model->getTagbyTagName($tag);
                if ($tag_result) {   // if exist tag in table tags
                    $tag_field[$i]['name'] = $tag_result->name;
                    $tag_field[$i]['uri'] = $tag_result->slug_url;
                } else {  //  if not exist tag in table tags
                    $tag_new['name'] = $tag_field[$i]['name'] = $tag;
                    $tag_new['slug_url'] = $tag_field[$i]['uri'] = str_slug($tag, "-");
                    // create new tag in table tags
                    $this->_tag_model->InsertTag($tag_new);
                }
                $params['tags'] = $tag_field;
                $i++;
            }
        }

        // GET field SEO
        $params['seo'] = array();
        if (isset($request->meta_title)) {
            $params['seo']['title'] = $request->meta_title;
        } else {
            $params['seo']['title'] = $request->post_name;
        }

        if (isset($request->meta_description)) {
            $params['seo']['description'] = $request->meta_description;
        } else {
            $params['seo']['description'] = $request->post_name;
        }
        if (isset($request->meta_keywords)) {
            $params['seo']['meta'] = $request->meta_keywords;
        } else {
            $params['seo']['meta'] = $request->post_name;
        }

        // Get User_review
        //$params['user_reviewed'] = '569867003d1138582e8b4567';

        // Get value lang code
        $params['lang_code'] = $request->lang_code;

        // Get value category
        $params['category_id'] = $request->category;

        if ($this->_post_model->UpdatePost($request->post_id, $params)) {
            \Session::flash('success', 'Edit Success!');
            return Redirect('dashboard/post');
        } else {
            \Session::flash('error', 'Error!');
            return Redirect('dashboard/post/editpost');
        }
    }

    public function postTag()
    {
        $tag_name = Input::get('data_post');
        $data = $this->_tag_model->getTaglikeTagName($tag_name, $this->tag_match_order, $this->tag_match_limit);
        $data = json_encode($data);
        return response()->json($data);
    }
    
    public function getReview()
    {
        $post_id = Input::get('post_id');
        $data = $this->_post_model->findPostbyId($post_id);
        $data = json_encode($data);
        return response()->json($data);
    }

    public function getIndex()
    {
        \Session::has('post_per_page') ? $page = \Session::get('post_per_page') : $page = 5;
        \Session::has('post_by_user') ? $user = \Session::get('post_by_user') : $user = 'all';
        \Input::has('search') ? $posts = $this->_post_model->getSearch(\Input::get('search'), $page, $user) : $posts = $this->_post_model->getAllPostPage($page, $user);
        return view('child.post.listpost')->with('posts', $posts);
    }

    // Action Delete, Active, Deactive Media
    public function anyActionPost()
    {
        if (!\Input::has('session') && !\Input::has('user')) {
            $check_post = \Input::get('listId');
            $action_post = \Input::get('select_action');
            if (isset($check_post)) {
                foreach ($check_post as $id) {
                    if (!empty($check_post) && $action_post) {
                        if ($action_post == 'Delete') {
                            $job = (new ActionContent($id, '\App\Models\Post', 'delete', 'post'))->delay(10);
                            $this->dispatch($job);
                            \Session::flash('success', 'Command delete added to queue . Please wait for minutes');
                        }
                        if ($action_post == 'Active') {
                            $this->_post_model->ActivePost($id, 1);
                            \Session::flash('success', 'Active Post Success');
                        }
                        if ($action_post == 'Deactive') {
                            $this->_post_model->ActivePost($id, 0);
                            \Session::flash('success', 'Deactive Post Success');
                        }
                    }
                }
            }
            if (\Input::get('type') == 'ajax') {
                return response()->json(['status' => true]);
            }
            return \Redirect::back();
        } else {
            if (!\Input::has('user')) {
                \Session::put('post_per_page', \Input::get('session'));
            } else {
                if (\Input::get('user') != 'all') {
                    \Session::put('post_by_user', \Input::get('user'));
                } else {
                    \Session::forget('post_by_user');
                }
            }
        }
    }

    public function getUpdateStatusPost($id, $status)
    {
        if ($status == 1) {
            $this->_post_model->ActivePost($id, 0);
            echo "success";
        } else {
            $this->_post_model->ActivePost($id, 1);
            echo "success";
        }
    }

    public function getDataPostReview()
    {
        $post_id = Input::get('post_id');
        $post = $this->_post_model->findPostbyId($post_id);
        if ($post == false) {
            abort(404);
        }
        return view('child.post.datapostreview')->with('post', $post);
    }
}