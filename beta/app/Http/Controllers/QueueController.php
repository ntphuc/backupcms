<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Requests;
use App\Models\Notification ; 
use App\Http\Controllers\Controller;
use App\Repositories\Post\PostRepository;
use App\Jobs\ReleaseContent ; 
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
class QueueController extends Controller
{
    use DispatchesJobs;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request,PostRepository $post ){
        $this->_request = $request ;
        $this->_post_model = $post;
    }
    public function anyIndex()
    {
        if(\Input::has('post_id')) {
            $post = $this->_post_model->findPostbyId(\Input::get('post_id'));
            $data =  [
                        'title' => $post->post_name ,
                        'content' => $post->post_content ,
                        'thumb' => $post->media_thumbnail ,
                        'desc' => $post->seo['description'] ,
                        'category_send' => [665] 
                      ]; 
        }
        $categories = \Input::get('listWeb') ; 
        foreach($categories as $items ) {
            $job = (new ReleaseContent($items , $data ))->delay(10);
            $this->dispatch($job) ; 
        }
        $this->_request->session()->flash('success' , 'Action added successfully') ;
        return \Redirect::back() ; 
    }
    public function anyNotification(){
        $noti = Notification::where('reload' , '=' , 'true')->first() ;
        if(isset($noti)) {
            \Session::flash('success' , 'Queue exec delete success') ;
            $noti->reload = 'false' ;
            $noti->save() ; 
            echo "reload" ;     
        }else{
            exit() ; 
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
