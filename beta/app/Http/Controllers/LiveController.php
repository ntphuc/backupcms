<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobs\ActionContent;
use App\Repositories\Post\PostRepository;
use App\Repositories\Media\MediaRepository;
use App\Repositories\Live\LiveRepository;

class LiveController extends Controller
{

    protected $_live_model;

    public $domain = 'http://125.212.193.90:2016';

    function __construct(PostRepository $post, MediaRepository $media, LiveRepository $live)
    {
        $this->_live_model = $live;
    }

    public function getIndex()
    {
        \Session::has('live_per_page') ? $page = \Session::get('live_per_page') : $page = 25;
        \Input::has('q') ? $data['live_datas'] = $this->_live_model->getSearch(\Input::get('q'), $page) : $data['live_datas'] = $this->_live_model->getAllLive($page);
        return view('child.live.listlive')->with($data);
    }

    public function getCreate()
    {
        return view('child.live.newlive');
    }

    public function postCreate()
    {
        $live = \Input::except('_token', 'result-home', 'result-away', 'live_now');

        // Chuyen anh logo ve sever minh
        if(strpos(\Input::get('homeTeamLogo'), $this->domain) == false) {       // Neu logo home team ko co tren serve minh
            $live['homeTeamLogo'] = $this->saveImageLogo(\Input::get('homeTeamLogo'), 'logo-home-team');
        }
        if(strpos(\Input::get('awayTeamLogo'), $this->domain) == false) {       // Neu logo away team ko co tren serve minh
            $live['awayTeamLogo'] = $this->saveImageLogo(\Input::get('awayTeamLogo'), 'logo-away-team');
        }

        $live['result']['goalsHomeTeam'] = \Input::get('result-home') != '' ? (int) \Input::get('result-home') : 0;
        $live['result']['goalsAwayTeam'] = \Input::get('result-away') != '' ? (int) \Input::get('result-home') : 0;
        $live['have_report'] = \Input::get('have_report') != '' ? true : false;
        $live['status'] = \Input::get('have_report') != '' ? 'TIMED' : 'FINISHED';
        $live['date'] = new \MongoDate(strtotime(\Input::get('date')));

        // Insert live
        $live_insert = $this->_live_model->InsertLive($live);
        if($live_insert != false) {
            if(\Input::has('live_now')) {
                return redirect('dashboard/live/event/' . $live_insert->id);
            }
            \Session::flash('success', 'Create live success!');
            return redirect('dashboard/live');
        } else {
            \Session::flash('success', 'Wrong when create live!');
            return redirect('dashboard/live/create');
        }
    }

    public function getEdit()
    {
        $live_id = \Input::get('live_id');
        $live = $this->_live_model->findLivebyId($live_id);
        if($live != null) {
            $live->date = date('Y-m-d H:m:s', $live->date->sec);
            $data = json_encode($live);
            return response()->json($data);
        } else {
            $data = [
                'status' => 'error'
            ];
        }
        return response()->json($data);
    }

    public function postEdit()
    {
        $live = \Input::except('_token','live_id', 'result-home', 'result-away', 'live_now');
        $live_id = \Input::get('live_id');
        $live_db = $this->_live_model->findLivebyId($live_id);
        if($live_db) {
            // Kiem tra logo co tren serve ko?
            if($live['homeTeamLogo'] != $live_db->homeTeamLogo) {       // thay doi logo home team
                if(strpos(\Input::get('homeTeamLogo'), $this->domain) == false) {       // Neu logo home team ko co tren serve minh
                    $live['homeTeamLogo'] = $this->saveImageLogo(\Input::get('homeTeamLogo'), 'logo-home-team');
                }
            }
            if($live['awayTeamLogo'] != $live_db->awayTeamLogo) {       // thay doi logo away team
                if (strpos(\Input::get('awayTeamLogo'), $this->domain) == false) {       // Neu logo away team ko co tren serve minh
                    $live['awayTeamLogo'] = $this->saveImageLogo(\Input::get('awayTeamLogo'), 'logo-away-team');
                }
            }

            $live['result']['goalsHomeTeam'] = \Input::get('result-home') != '' ? (int) \Input::get('result-home') : 0;
            $live['result']['goalsAwayTeam'] = \Input::get('result-away') != '' ? (int) \Input::get('result-home') : 0;
            $live['have_report'] = \Input::get('have_report') != '' ? true : false;
            $live['status'] = \Input::get('have_report') != '' ? 'TIMED' : 'FINISHED';
            $live['date'] = new \MongoDate(strtotime(\Input::get('date')));

            if($this->_live_model->UpdateLive($live_id, $live)) {
                \Session::flash('success', 'Updated!');
                return redirect('dashboard/live');
            }
        }
        \Session::flash('error', 'Error!');
        return redirect()->route('live');
    }

    public function getTableReviewLive() {
        $live_id = \Input::get('live_id');
        $data['live'] = $this->_live_model->findLivebyId($live_id);
        return view('child.live.tablereviewlive')->with($data);
    }

    public function getEvent($live_id)
    {
        $data['live'] = $this->_live_model->findLivebyId($live_id);
        if(!$data['live']) {
            abort(404);
        }
        if($data['live']->have_report != true) {    // If no report then return listlive
            \Session::flash('error', 'This live no report!');
            return  redirect('dashboard/live');
        }
        return view('child.live.event')->with($data) ;
    }

    public function postEvent() {
        $events = \Input::except('live_id', '_token', 'event_key');
        $live_id = \Input::get('live_id');

        if($this->_live_model->InsertEvent($live_id,$events)) {
            \Session::flash('success', 'Create event success!');
            return redirect('dashboard/live/event/' . $live_id);
        }
        \Session::flash('error', 'Exist an event in this time or match finished!');
        return redirect('dashboard/live/event/' . $live_id);
    }

    public function anyViewListLive() {
        \Session::has('live_per_page') ? $page = \Session::get('live_per_page') : $page = 25;
        \Input::has('q') ? $data['live_datas'] = $this->_live_model->getSearch(\Input::get('q'), $page ) : $data['live_datas'] = $this->_live_model->getAllLive($page);
        return view('child.live.listlive')->with($data);
    }

    public function anyActionLive() {
        if(!\Input::has('live_per_page') && !\Input::has('time')) {
            $check_live = \Input::get('listId');
            $action_live = \Input::get('select_action_2') != '' ? \Input::get('select_action_2') : \Input::get('select_action');
            if(!empty($check_live) && $action_live) {
                if($action_live == 'Delete') {
                    foreach($check_live as $id) {
                        $job = (new ActionContent($id, '\App\Models\LiveSchedule', 'delete', 'live'))->delay(10);
                        $this->dispatch($job);
                        \Session::flash('success', 'Command delete added to queue . Please wait for minutes');
                    }
                } elseif ($action_live == 'live') {
                    foreach($check_live as $id) {
                        $this->_live_model->live($id);
                    }
                } elseif ($action_live == 'dislive') {
                    foreach($check_live as $id) {
                        $this->_live_model->disLive($id);
                    }
                }
            }
            if(\Input::get('type') == 'ajax') {
                return response()->json(['status' => true]);
            }
            return \Redirect::back();
        } else {
            if(!\Input::has('time')) {
                \Session::put('live_per_page',\Input::get('live_per_page'));
            } else {
                \Session::put('time',\Input::get('time'));
            }
        }
    }

    public function getEditEvent() {
        $live_id = \Input::get('live_id');
        $event_key = \Input::get('event_key');
        $live = $this->_live_model->findLivebyId($live_id);
        if($live->events[$event_key] != null) {
            $data = json_encode($live->events[$event_key]);
            return response()->json($data);
        } else {
            $data = [
                'status' => 'error'
            ];
        }
        return response()->json($data);
    }

    public function postEditEvent() {
        $event = \Input::except('live_id', 'event_key', '_token');
        $live_id = \Input::get('live_id');
        $event_key = \Input::get('event_key');
        if($this->_live_model->UpdateEvent($live_id,$event_key,$event)) {
            \Session::flash('success', 'Update event success!');
            return redirect('dashboard/live/event/' . $live_id);
        }
        \Session::flash('error', 'Exist an event in this time or some thing when wrong!');
        return redirect('dashboard/live/event/' . $live_id);
    }

    public function postDeleteEvent() {
        $event_key = (int) \Input::get('event_key');
        $live_id = \Input::get('live_id');
        if($this->_live_model->DeleteEvent($live_id, $event_key)) {
            $data = [
                'status'    => true,
                'messenger' => 'Deleted!'
            ];
        } else {
            $data = [
                'status' => false,
                'messenger' => 'Delete error!'
            ];
        }
        return response()->json($data);
    }

    public function saveImageLogo($logo, $team) {   // Chuyen anh logo ve sever minh

        if (exif_imagetype($logo) == IMAGETYPE_GIF) $image_thumb_type = '.gif';
        elseif (exif_imagetype($logo) == IMAGETYPE_JPEG) $image_thumb_type = '.jpeg';
        elseif (exif_imagetype($logo) == IMAGETYPE_PNG) $image_thumb_type = '.png';
        else $image_thumb_type = '.jpeg';
        $logo_file = file_get_contents($logo);
        $file_path = env('upload_dir') . $team . '-' .date('d-m-Y-H-m-s') .$image_thumb_type;
        file_put_contents( $file_path ,$logo_file);
        return $file_path;
    }
}