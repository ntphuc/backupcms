<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Helpers\FileUpload ;
use App\Helpers\FroalaHelper;
class UploadController extends Controller
{
    function anyUploadFile()
    {

        $upload_dir = env('upload_dir').env('folder_image');

        $uploader = new FileUpload();

        $result = $uploader->handleUpload($upload_dir);
        $mime =  mime_content_type($result) ;

        if (!$result) {
            exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));
        }

        echo json_encode(array('success' => true , 'file_path' => env('link_static').str_replace(env('folder_remove') , '' , $result), 'file_where' => $result , 'mime' => $mime  )  );

    }
    //FROALA UPLOAD MEDIA
    function getAjaxLoadImage()
    {
        $froala = new FroalaHelper(env('link_static'), env('folder_image'), env('upload_dir'), env('URL'));
        $respone = $froala->LoadImageFroala();
        print_r($respone);

    }

    function anyAjaxUploadImage()
    {
        $froala = new FroalaHelper(env('link_static'), env('folder_image'), env('upload_dir'), env('URL'));
        $respone = $froala->UploadImageFroala();
        print_r($respone);
    }
}