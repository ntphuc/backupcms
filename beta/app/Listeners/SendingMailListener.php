<?php

namespace App\Listeners;

use App\Events\SendingMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendingMailListener implements ShouldQueue 
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendingMail  $event
     * @return void
     */
    public function handle(SendingMail $event)
    {
        $event->broadcastOn() ;
    }
}
