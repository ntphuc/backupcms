<?php

namespace App\Listeners;

use App\Events\ProcQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcQueueListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProcQueue  $event
     * @return void
     */
    public function handle(ProcQueue $event)
    {
        $event->broadcastOn() ;
    }
}
