<?php
namespace App\Helpers;
class FroalaHelper
{
    public $link_static;
    public $folder_image;
    public $folder_upload;
    public  $url ;

    function __construct($link_static, $folder_image, $folder_upload , $url )
    {
        $this->link_static = $link_static;
        $this->folder_image = $folder_image;
        $this->folder_upload = $folder_upload;
        $this->url = $url ;

    }
    function  UploadImageFroala(){
        \Debugbar::disable();
        // Allowed extentions.
        $allowedExts = array("gif", "jpeg", "jpg", "png");

        // Get filename.
        $temp = explode(".", $_FILES["file"]["name"]);

        // Get extension.
        $extension = end($temp);

        // An image check is being done in the editor but it is best to
        // check that again on the server side.
        // Do not use $_FILES["file"]["type"] as it can be easily forged.
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

        if ((($mime == "image/gif")
                || ($mime == "image/jpeg")
                || ($mime == "image/pjpeg")
                || ($mime == "image/x-png")
                || ($mime == "image/png"))
            && in_array($extension, $allowedExts)) {
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            // Save file in the uploads folder.
            move_uploaded_file($_FILES["file"]["tmp_name"], $this->folder_upload.$this->folder_image . $name);

            // Generate response.
            $response = new \StdClass;
            $response->link = str_replace($this->url , '' , $this->link_static).$this->folder_image . $name;
            echo stripslashes(json_encode($response));
        }
    }

    function LoadImageFroala()
    {
        $response = array();
        $image_types = array(
            "image/gif",
            "image/jpeg",
            "image/pjpeg",
            "image/jpeg",
            "image/pjpeg",
            "image/png",
            "image/x-png"
        );
        $fnames = scandir($this->folder_upload.$this->folder_image);
        if ($fnames) {
            foreach ($fnames as $name) {
                if (!is_dir($name)) {
                    if (in_array(mime_content_type($this->folder_upload.$this->folder_image . $name), $image_types)) {
                        // Build the image.
                        $img = new \StdClass;
                        $img->url = $this->link_static . $this->folder_image . $name;
                        $img->thumb = $this->link_static . $this->folder_image . $name;
                        $img->name = $name;
                        $img->type = 'image';
                        array_push($response, $img);
                    }
                }
            }
        }
        else {
            $response = new \StdClass;
            $response->error = "Images folder does not exist!";
        }

        $response = json_encode($response);
        return $response ;
    }
}