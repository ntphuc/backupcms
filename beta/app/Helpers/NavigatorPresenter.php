<?php
/**
 * Created by PhpStorm.
 * User: ger2017
 * Date: 11/21/15
 * Time: 2:02 AM
 */

namespace App\Helpers;

use Pingpong\Menus\Presenters\Presenter;

class NavigatorPresenter extends Presenter
{
    /**
     * {@inheritdoc }
     */
    public function getOpenTagWrapper()
    {
        return PHP_EOL . '<nav class="navigation"><ul class="list-unstyled">' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getCloseTagWrapper()
    {
        return PHP_EOL . '</ul></nav>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        if ($this->getActiveState($item) !== null) {
            $url = 'javascript:void(0);';
        } else {
            $url = $item->getUrl();
        }
        return '<li' . $this->getActiveState($item) . '><a href="' . $url . '">' . $item->getIcon() . '<span class="nav-label">' . $item->title . '</span></a></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getActiveState($item)
    {
        return \Request::is($item->getRequest()) ? ' class="active"' : null;
    }

    /**
     * {@inheritdoc }
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithDropDownWrapper($item)
    {
        return '<li class="has-submenu">
                <a href="#">
                 ' . $item->getIcon() . '<span class="nav-label">' . $item->title . '</span>
                </a>
                <ul class="list-unstyled">
                  ' . $this->getChildMenuItems($item) . '
                </ul>
              </li>' . PHP_EOL;
    }
}