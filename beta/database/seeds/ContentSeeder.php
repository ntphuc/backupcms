<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "* START SEEDING 3 USER.....\n";
        $user = factory(\App\Models\User::class, 2)->create()->each(function ($user) {
            echo "* USER_ID = " . $user->_id . ". START SEEDING 5 CATEGORY.....\n";
            $category = factory(\App\Models\Category::class, 5)->create()->each(function ($category) use ($user) {
                echo "  => CATEGORY_ID = " . $category->_id . ":\n";
                /**
                 * 50 posts / 1 category
                 */
                echo "      => SEEDING 50 POSTS";
                $posts = factory(\App\Models\Post::class, 50)->create(['user_id' => $user->_id , 'user_reviewed' => $user->_id , 'category_id' => [$category->_id]])->each(function () {
                    echo " . ";
                });
                echo "DONE\n";
                /**
                 * 50 films / 1 category
                 */
//                echo "      => SEEDING 50 FILMS";
                factory(\App\Models\Tags::class, 50)->create();
                echo "DONE\n";
            });

            echo "* USER_ID = " . $user->id . ". SEEDING 50 MEDIA";
            $medias = factory(\App\Models\Media::class, 50)->create(['user_id' => $user->_id])->each(function () {
                echo " . ";
            });
            echo "DONE\n";
//
//            echo "* USER_ID = " . $user->id . ". SEEDING 50 LIVE_SCHEDULE:";
//            $live_schedule = factory(\App\Models\LiveSchedule::class, 50)->create(['user_id' => $user->id])->each(function ($match) use ($user) {
//                echo "  => LIVE_SCHEDULE = " . $match->id . ":\n";
//                /**
//                 * 30 events per match
//                 */
//                echo "      => SEEDING 30 EVENTS: ";
//                $match->events()->saveMany(
//                    factory(\App\Models\LiveEvent::class, 30)->create(['user_id' => $user->id, 'match_id' => $match->id])->each(function () {
//                        echo " . ";
//                    })
//                );
//                echo "DONE\n";
//            });
        });
    }
}
