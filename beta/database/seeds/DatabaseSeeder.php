<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * call the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        /**
         * Default user admin/mod
         */
//        $this->call(UserSeeder::class);

        /**
         * Content
         */
        $this->call(ContentSeeder::class);

//        $live_schedule = factory(\App\Models\LiveSchedule::class, 50)->create(['user_id' => 1])->each(function ($match) {
//            echo "  => LIVE_SCHEDULE = " . $match->id . ":\n";
//            /**
//             * 30 events per match
//             */
//            echo "      => SEEDING 30 EVENTS: ";
//            $match->events()->saveMany(
//                factory(\App\Models\LiveEvent::class, 30)->create(['user_id' => 1, 'match_id' => $match->id])->each(function () {
//                    echo " . ";
//                })
//            );
//            echo "DONE\n";
//        });

        // Option demo
        $main_menu = new \App\Models\Option();
        $main_menu->site_name = 'kqtt.vn';
        $main_menu->meta_key = 'main_menu';
        $main_menu->meta_value = '[{"url":"\/","title":"Trang ch\u1ee7","icon":"fa fa-home"},{"url":"\/","title":"Tin T\u1ee9c","icon":"fa fa-newspaper-o"},{"url":"\/","title":"T\u01b0 v\u1ea5n","icon":"fa fa-flag-checkered"},{"url":"\/","title":"T\u01b0\u1eddng thu\u1eadt","icon":"fa fa-calendar"},{"url":"\/","title":"Video High light","icon":"fa fa-play"},{"url":"\/","title":"Tennis","icon":"fa fa-paper-plane-o"}]';
        $main_menu->save();

        $home_layout = new \App\Models\Option();
        $home_layout->site_name = 'kqtt.vn';
        $home_layout->meta_key = 'home_layout';
        $home_layout->meta_value = '[{"widget_type":"recent_post","widget_style":"style_1","widget_title":"Cliphot m\u1edbi nh\u1ea5t","category_id":1,"number_of_item":9},{"widget_type":"advertise","widget_style":"width_720","widget_title":"Advertise Here"},{"widget_type":"recent_post","widget_style":"style_2","widget_title":"Cliphot m\u1edbi nh\u1ea5t","category_id":2,"number_of_item":9},{"widget_type":"advertise","widget_style":"width_720","widget_title":"Advertise Here"},{"widget_type":"recent_post","widget_style":"style_3","widget_title":"Cliphot m\u1edbi nh\u1ea5t","category_id":1,"number_of_item":9}]';
        $home_layout->save();

        $sidebar_layout = new \App\Models\Option();
        $sidebar_layout->site_name = 'kqtt.vn';
        $sidebar_layout->meta_key = 'sidebar_layout';
        $sidebar_layout->meta_value = '[{"widget_type":"sidebar_popular_post","widget_title":"Cliphot m\u1edbi nh\u1ea5t","category_id":1,"number_of_item":9},{"widget_type":"sidebar_advertise","widget_title":"Advertise Here"}]';
        $sidebar_layout->save();

        $footer_layout = new \App\Models\Option();
        $footer_layout->site_name = 'kqtt.vn';
        $footer_layout->meta_key = 'footer_layout';
        $footer_layout->meta_value = '[{"widget_type":"footer_html","widget_title":"About site", "content":"<a href=\"index.html\"><img src=\"images\/footer-logo.png\" alt=\"\"><\/a><br>\n\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut\n            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\n            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in\n            voluptate velit<\/p>"},{"widget_type":"footer_tag_list","widget_title":"Popular Tag"}]';
        $footer_layout->save();

        Model::reguard();
    }
}
